<div class="row">
    <div class="small-1 medium-1 columns">&nbsp;</div>
    <div class="small-3 medium-3 columns"><h3>Items Needed</h3></div>
    <div class="small-3 medium-3 columns"><h3>Quantity</h3></div>
    <div class="small-3 medium-3 columns"><h3>Notes</h3></div>
</div>

<?php 
  $url = "http://infocus.bridgeheadstudios.com/backoffice/public/api/collateral";
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
  $response = curl_exec($ch);
  curl_close($ch);
  $formItems = json_decode($response);
?>

<?php
$formItemss = array(
  'Inspire Education K12',
  'Inspire Higher Education',
  'Mondopad Family',
  'JTouch Family',
  // 'JTouch 7530 Datasheet',
  // 'Touchscreen Solutions datasheet',
  'Touch Display Comparisons',
  // 'JTouch Table Flyer',
  // 'Big Touch Family',
  // 'Wireless Projection Features',
  'Projector Reference Guide',
  '5148 Projector datasheet',
  'IN110V Projector Datasheet',
  'IN120STx Projector datasheet',
  '120x Projector datasheet',
  '2120x Projector datasheet',
  'Catalyst 4K',
  // 'Control 1000',
  // 'Catalyst 4500-B',
  // 'Catalyst 4500-C',
  // 'Catalyst 4500-H',
  'C-Series data sheet',
  'Canvas CRS 4K',
  'Canvas',
  // 'Canvas Touch',
  'Pixel Net',
  // 'ConX Wall',
  'ConX Exec',
  'Virtual Demo Flyer',
  'Monthly Incentives flyer',
  'Misc Channel Flyer (Please list name)',
  'Corporate Brochure', 'InFocus 2 Pocket Folder',
  'Pens', 'Giveaways Bottle openers', 'Giveaways Chap sticks',
  'Table Throw/Skirt White logo',
  // 'IN5148HDLC projector',
  // 'jTouch Interactive display',
  // 'Mondopad',
  // 'Canvas Touch Banner',
  // 'DigiEasel Interactive display',
  'Touch Display Sign',
  'Projectors Sign',
  'Jupiter 4K Processor Sign',
  'Control 1000 Sign',
  'Canvas Banner Sign',
  'Conx',
);

$costCenters = array(
    'Jupiter by InFocus 3400',
    'InFocus North America 3337',
    'InFocus EMEA 3533',
    'Infocus APAC 3349',
  );

$placeholder = "./rw_common/themes/tesla_pro/images/thumbs/px500/placeholder.png";
?>

<?php foreach($formItems as 
  $key => 
  $formItem): ?>

<?php 
  $name   = str_replace(" ", "_", $formItem->name);
  $src    = "./rw_common/themes/tesla_pro/images/thumbs/px500/" . str_replace([" ", "/"], ["-","+"], $formItem->name) . "_lg.png";

  $thumb  = "./rw_common/themes/tesla_pro/images/thumbs/px32/" . str_replace([" ", "/"], ["-","+"], $formItem->name) . "_sm.png";

  $name = $formItem->name;
  $src = $formItem->full_lg_url;
  $thumb = $formItem->full_sm_url;

?>
  <div class="row">
    <div class="small-1 medium-1 columns">
          <img class="lets-do-this" src="<?php echo $thumb ?>?<?php echo rand(199,9999);?>" data-big="<?php echo $src ?>?<?php echo rand(199,9999);?>" data-big2x="<?php echo $src ?>" width="40" height="40" />
    </div>
    <div class="small-3 medium-3 columns">
      <h5>
        <?php echo $formItem->name ?>
      </h5>
    </div>
    <div class="small-3 medium-3 columns">
      <label id="item1<?php echo ($key + 3) ?>_label_0">
        <input type="number" placeholder="Qty" id="item1<?php echo ($key + 3) ?>_number_1" class="form-control" autocomplete="off" min="0" max="999999999" step="1" data-hint="" name="qty_<?php echo $name ?>">
      </label>
    </div>
    <div class="small-3 medium-3 columns">
      <label id="item1<?php echo ($key + 3) ?>_label_0">
        <textarea id="item1<?php echo $key ?>_textarea_1" placeholder="Notes" class="form-control" maxlength="10000" placeholder="" data-hint="" name="notes_<?php echo $name ?>"></textarea>
      </label>
    </div>
  </div>
<?php endforeach; ?>

<div class="row">
  <div class="small-8 medium-8 columns">
    <label>Comments
        <textarea placeholder="If a specific literature piece is not listed on the form please enter your request here and we will do our best to fulfill it?" style="resize: none; max-width: 960px;  font-size: 13px; font-weight: 300; height: 80px;" id="item96_textarea_1" maxlength="10000" placeholder="" data-hint="" name="comments"></textarea>
    </label>
  </div>
</div>

<div class="row">
  <div class="small-8 medium-8 columns">
    <h5>
      Date Needed
    </h5>
  </div>
</div>

<div class="row">
  <div class="small-8 medium-8 columns">
    <label id="item98_label_0">*Select a date
      <input type="text" class="datepicker no-margin-bottom" id="item98_date_1" data-hint="" name="date_needed" required="">
      <span class="error-msg"></span>
    </label>
  </div>
</div><br />

<div class="row">
  <div class="small-8 medium-8 columns">
    <label id="item99_label_1">*Event Name
      <input type="text" class="no-margin-bottom" id="event_name" data-hint="" name="event_name" required="">
      <span class="error-msg"></span>
    </label>
  </div>
</div><br />

<div class="row">
  <div class="small-8 medium-8 columns">
    <label id="item99_label_1">*Requested By
      <input type="text" class="no-margin-bottom" id="requested_by" data-hint="" name="requested_by" placeholder="Name" required="">
      <span class="error-msg"></span>
    </label>
  </div>
</div><br />

<div class="row">
  <div class="small-8 medium-8 columns">
    <label id="item99_label_1">Cost center
      <select class="form-select-field" name="cost_center">
          <option>Please select one option</option>
        <?php foreach ($costCenters as $costCenter): ?>
          <option value="<?php echo $costCenter;?>"><?php echo $costCenter;?></option>
        <?php endforeach; ?>
      </select>

      <span class="error-msg"></span>
    </label>
  </div>
</div><br />

<div class="row">
  <div class="small-8 medium-8 columns">
    <h5>
      Ship To:
    </h5>
  </div>
</div>

<?php
  $formData = array(
      'company'     => 'Company',
      'email'       => 'Email Address',
      'attn'        => 'Attn.',
      'street'      => 'Street',
      'city'        => 'City',
      'province'    => 'Province',
      'state'       => 'State',
      'zip'         => 'Zip',
      'phone'       => 'Phone'
  );

  foreach ($formData as $key => $value) {
?>

    <div class="row">
      <div class="small-10 medium-10 columns">
        <label <?php echo ($key == 'state')? 'id="state_label"' : '' ?>>
            <?php echo ($key !== 'province') ? '*' : ''?><?php echo $value ?>
            <input type="text" id="item102_text_1" class="no-margin-bottom" maxlength="254" placeholder="" autocomplete="off" data-hint="" name="<?php echo $key ?>" required="">
            <?php echo ($key == 'state')? 'Required if United States of America is selected' : '' ?>
            <span class="error-msg"></span>
        </label>
      </div>
    </div><br />

    <?php
    if($key == 'state')
    {
      $countries = array('Afganistan','Albania','Algeria','American Samoa','Andorra','Angola','Anguilla',
      'Antigua &amp; Barbuda','Argentina','Armenia','Aruba','Australia','Austria','Azerbaijan','Bahamas',
      'Bahrain','Bangladesh','Barbados','Belarus','Belgium','Belize','Benin','Bermuda','Bhutan','Bolivia',
      'Bonaire','Bosnia &amp; Herzegovina','Botswana','Brazil','British Indian Ocean Ter','Brunei','Bulgaria',
      'Burkina Faso','Burundi','Cambodia','Cameroon','Canada','Canary Islands','Cape Verde','Cayman Islands',
      'Central African Republic','Chad','Channel Islands','Chile','China','Christmas Island','Cocos Island',
      'Colombia','Comoros','Congo','Cook Islands','Costa Rica','Cote DIvoire','Croatia','Cuba','Curaco','Cyprus',
      'Czech Republic','Denmark','Djibouti','Dominica','Dominican Republic','East Timor','Ecuador','Egypt',
      'El Salvador','Equatorial Guinea','Eritrea','Estonia','Ethiopia','Falkland Islands','Faroe Islands','Fiji',
      'Finland','France','French Guiana','French Polynesia','French Southern Ter','Gabon','Gambia','Georgia',
      'Germany','Ghana','Gibraltar','Great Britain','Greece','Greenland','Grenada','Guadeloupe','Guam','Guatemala',
      'Guinea','Guyana','Haiti','Hawaii','Honduras','Hong Kong','Hungary','Iceland','India','Indonesia','Iran','Iraq',
      'Ireland','Isle of Man','Israel','Italy','Jamaica','Japan','Jordan','Kazakhstan','Kenya','Kiribati','Korea North',
      'Korea Sout','Kuwait','Kyrgyzstan','Laos','Latvia','Lebanon','Lesotho','Liberia','Libya','Liechtenstein',
      'Lithuania','Luxembourg','Macau','Macedonia','Madagascar','Malaysia','Malawi','Maldives','Mali','Malta',
      'Marshall Islands','Martinique','Mauritania','Mauritius','Mayotte','Mexico','Midway Islands','Moldova','Monaco',
      'Mongolia','Montserrat','Morocco','Mozambique','Myanmar','Nambia','Nauru','Nepal','Netherland Antilles',
      'Netherlands','Nevis','New Caledonia','New Zealand','Nicaragua','Niger','Nigeria','Niue','Norfolk Island',
      'Norway','Oman','Pakistan','Palau Island','Palestine','Panama','Papua New Guinea','Paraguay','Peru',
      'Phillipines','Pitcairn Island','Poland','Portugal','Puerto Rico','Qatar','Republic of Montenegro',
      'Republic of Serbia','Reunion','Romania','Russia','Rwanda','St Barthelemy','St Eustatius','St Helena',
      'St Kitts-Nevis','St Lucia','St Maarten','St Pierre &amp; Miquelon','St Vincent &amp; Grenadines','Saipan',
      'Samoa','Samoa American','San Marino','Sao Tome &amp; Principe','Saudi Arabia','Senegal','Serbia','Seychelles',
      'Sierra Leone','Singapore','Slovakia','Slovenia','Solomon Islands','Somalia','South Africa','Spain','Sri Lanka',
      'Sudan','Suriname','Swaziland','Sweden','Switzerland','Syria','Tahiti','Taiwan','Tajikistan','Tanzania','Thailand',
      'Togo','Tokelau','Tonga','Trinidad &amp; Tobago','Tunisia','Turkey','Turkmenistan','Turks &amp; Caicos Is','Tuvalu',
      'Uganda','Ukraine','United Arab Erimates','United Kingdom','United States of America','Uraguay','Uzbekistan','Vanuatu',
      'Vatican City State','Venezuela','Vietnam','Virgin Islands (Brit)','Virgin Islands (USA)','Wake Island',
      'Wallis &amp; Futana Is','Yemen','Zaire','Zambia','Zimbabwe');
?>
      <div class="row">
        <div class="small-10 medium-10 columns">
          <label>*Country</label>
          <select class="form-select-field" name="country" id="country">
            <option value="">Select one</option>
            <?php foreach ($countries as $country) {
              echo "<option value='" . $country . "'>" . $country . "</option>";
            } ?>
          </select><br /><span class="error-msg"></span><br />
        </div>
      </div>

<?php } } ?>

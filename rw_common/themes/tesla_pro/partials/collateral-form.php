<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Start of the headers for CoffeeCup Web Form Builder -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="rw_common/themes/tesla_pro/css/foundation.css">
    <link rel="stylesheet" href="rw_common/themes/tesla_pro/css/app.css">



    <!-- End of the headers for CoffeeCup Web Form Builder -->
    <title>Infocus Collateral Form</title>

    <script type="text/javascript"  src="rw_common/themes/tesla_pro/js/jquery.min.js"></script>
    <script type="text/javascript" src="rw_common/themes/tesla_pro/js/libs_js/jquery-ui-1.8.9.custom.min.js"></script>
    <script type="text/javascript" src="rw_common/themes/tesla_pro/js/libs_js/jquery.ui.datepicker.js"></script>
    <script type="text/javascript" src="rw_common/themes/tesla_pro/js/lang/messages_datepicker.js"></script>
    <script src="rw_common/themes/tesla_pro/js/libs_js/jquery.mlens-1.6.min.js" type="text/javascript"></script>

    <!-- <script src="rw_common/themes/tesla_pro/js/vendor/foundation.js"></script> -->
    <script src="rw_common/themes/tesla_pro/js/app.js"></script>

    <style media="screen">

        body {
            background-color: rgb(246,246,246);
        }

        form .row .columns {
            float: left;
        }

        .column:last-child:not(:first-child), .columns:last-child:not(:first-child) {
            float: left;
        }

        form .row img {
            cursor: pointer;
        }

        form .row .columns textarea {
            max-width: 100%;
            min-width: 220px;
        }
    </style>

  </head>

  <body>
    <div class="row" style="width: 100%; margin-left:2em;">
      <form id="docContainer" enctype="multipart/form-data" method="POST" action="./" novalidate="novalidate" class="" data-form="publish">

      <div class="row">
        <div class="small-12 medium-12 text-center columns" style="margin-bottom:20px;">
            <h2>Collateral Marketing Request Form</h2>
        </div>
      </div>

      <?php include('./collateral-form-common.php'); ?>

      <div id="fb-submit-button-div" class="fb-item-alignment-left fb-footer" style="min-height:0px;">
        <input type="submit" class="button radius" id="fb-submit-button" data-regular="" value="Submit" />
      </div>

      </form>
    </div>
    <script type="text/javascript">
    $(document).ready(function()
    {
      $("img.zoom").mlens(
      {
          imgSrc: $("img.zoom").attr("data-big"),	  // path of the hi-res version of the image
        //   imgSrc2x: $("#gear").attr("data-big2x"),  // path of the hi-res @2x version of the image
                                                    //for retina displays (optional)
          lensShape: "square",                // shape of the lens (circle/square)
          lensSize: ["100%","100%"],            // lens dimensions (in px or in % with respect to image dimensions)
                                              // can be different for X and Y dimension
          borderSize: 2,                  // size of the lens border (in px)
          borderColor: "#000",            // color of the lens border (#hex)
          borderRadius: 0,                // border radius (optional, only if the shape is square)
        //   imgOverlay: $("#gear").attr("data-overlay"), // path of the overlay image (optional)
          overlayAdapt: true,    // true if the overlay image has to adapt to the lens size (boolean)
          zoomLevel: 1,          // zoom level multiplicator (number)
          responsive: true       // true if mlens has to be responsive (boolean)
      });
    });
    </script>
  </body>
</html>

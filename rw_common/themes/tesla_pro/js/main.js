(function ($) {
	MAIN = {
		init: function(){
			if($('.rw-contact-form')){
				this.sendEvent();
				this.sendCollateral();
			}
		},

		sendEvent: function(){
			var that = this;
			$(document).on('submit','.rw-contact-form', function( event ) {
				event.preventDefault();

				$('body').append('<div class="popup-img full-height"><span class="close-me">X</span><img src="./rw_common/themes/tesla_pro/images/loader.gif"></img></div>');
				console.log('something', $('body div.popup-img'));

				var $submitEvent = $(this).find("input[type=submit]");
				$submitEvent.prop("disabled", true);

				//clean the errors
				$("input,select,textarea").removeClass("glowing-border");
				$(".rw-contact-form .error-msg").text("");

				var errorPointer;

				var data = $(".rw-contact-form").serializeArray().reduce(function(obj, item) {


					if (obj == undefined) {
						return;
					}

					if(item.name == "product_displayed" ||
					   item.name == "marketing_item" ||
					   item.name == "additional_items" ||
					   item.name == "additional_items_signage" ||
					   item.name == "demo_equipment" ||
					   item.name == "accessories")
					{
						var value;

						if(obj[item.name] == undefined)
						{
							obj[item.name] = [];
						}
						if (item.name == "product_displayed") {
							if ($("#product_displayed_model").val().length == 0) {
								$("#product_displayed_model ~ span:first").text("A Model No. should be provided");
								$submitEvent.prop("disabled", false);
								errorPointer = "#product_displayed_model";
								return;
							} else {
								value = item.value + ": " + $("#product_displayed_model").val();
							}
						} else if (item.name == "demo_equipment") {
							$input = $("input[type=text][data-rel='"+item.value+"']");

							if ($input.val() !== "") {
								value = item.value + ": " + $input.val();
							} else {
								$("#demo-equipment-error").text("There are one or more models not specified");
								$submitEvent.prop("disabled", false);
								errorPointer = "#demo-equipment-error";
								return;
							}
						} else if (item.name == "additional_items" && item.value == "Signage") {
							if ($("input[name=additional_items_signage]:checked").length > 0) {
								value = item.value;
							} else {
								console.log("wrong");
								$("#additional_items-error").text("If Signage is selected you should select at least one element");
								$submitEvent.prop("disabled", false);
								errorPointer = "input[type=checkbox][name=additional_items_signage]:last";
								value = item.value;
								return;
							}
						} else if (item.value == "Other") {
							value = item.value + ": " + $("#marketing_item_other").val();
						} else {
							value = item.value;
						}
						obj[item.name].push(value);
					}
					else
					{
						obj[item.name] = item.value;
					}

				    return obj;
				}, {});

				if (data != undefined) {
					$.ajax({
						method: "POST",
						type: "POST",
						url: "./backoffice/public/api/save-infocus-event",
						data: data,
						dataType: 'json',
						success: function(dataSuccess,status,xhr){
							if (dataSuccess.event_status_code && dataSuccess.event_status_code == 200) {
								// We send a nice popup alert telling the user the information was uploaded
								that.showMessage(dataSuccess.message);
								window.setTimeout(function () {
									document.location.reload();
								}, 3000);
							}
						},
						error: function( data ){
							var errors = JSON.parse(data.responseText);
							var first = true;

							$('body div.popup-img').remove();

					        $.each(errors, function(i, item) {
					        	$('input[name="' + i + '"]').addClass('glowing-border');
					         	$('input[name="' + i + '"] ~ span.error-msg:first').text(item[0]);

					         	$('select[name="' + i + '"]').addClass('glowing-border');
					         	$('select[name="' + i + '"] ~ span:first').text(item[0]);

					         	$('textarea[name="' + i + '"]').addClass('glowing-border');
					         	$('textarea[name="' + i + '"] ~ span:first').text(item[0]);
								if (first) {
									$('input[name="' + i + '"]').focus();
								}
								first = false;
					        });

							$submitEvent.prop("disabled", false);
						}
					});
				} else {
					$(".popup-img.full-height").remove();
					$(errorPointer).focus();
				}

			});

			$(document).on('click','#logout',function( event ) {
			    event.preventDefault();
                $.ajax({
					method: "GET",
					url: "./backoffice/public/api/form-logout",
					dataType: 'json',
					success: function(response,status,xhr) {
						document.location.reload();
					},
					error: function( data ) {
						console.log(data);
					}
				});
            });

			$(document).on('change','input[name=product_displayed]',function( event ) {
			    event.preventDefault();
				console.log(event);
                if (event.target.value) {

				}
            });

			$(document).on("click", "#collateral-form", function(event){
				$(this).modal({
					modalClass:"modal collateral-form"
				});

				window.setTimeout(function(){
					$( "#item98_date_1" ).datepicker();
					// $( "#item98_date_1" ).datetimepicker();
					// $("img.zoom").elevateZoom();
					// $("#thisGuy").elevateZoom();
			    }, 1500);

				return false;
			});

			$(document).on('click', 'img.lets-do-this',	function(event) {
				$('#docContainer').append('<div class="popup-img"><span class="close-me">X</span><img src="'+$(this).data("big")+'"></img></div>');
			});

			$(document).on('click', 'div.popup-img',	function(event) {
				$(this).remove();
			});

			$(document).on("click", "input[type=checkbox][name=demo_equipment]", function(event) {
				// get this element
				$this = $(this);
				if ($this.is(":checked")) {
					// text input with rel to value
					$input = $("<input type='text' data-rel='"+$this.val()+"'/>").addClass("form-input-field").css("width", "85px");
					// just text span
					$text = $("<span> Model Number needed/Comments: </span>").css("padding-left", "1em").append($input);
					// insert left on this
					$this.next().after($text);
				} else {
					console.log($this.next().next());
					$this.next().next().remove();
				}

			});
		},

		showMessage: function(message) {
			$elixir('body div.popup-img').css("background-color", "#fff").css("top", "15%");
			$elixir('body div.popup-img img').prop('src', './rw_common/themes/tesla_pro/images/subgraphic.png');
			// $elixir('#myNotificationsPopup .message').html(message);
			// $elixir('#myNotificationsPopup').modal();
		},

		sendSubmit: function(data){

			$.ajax({
				method: "POST",
				type: "POST",
				url: "./files/mailer.php",
				data: data,
				success: function(dataSuccess,status,xhr){
					location.reload();
				}
			});
		},

		getFormData: function(){

			$.ajax({
				method: "GET",
				type: "GET",
				url: "./backoffice/public/api/get-form-data",
				success: function(data,status,xhr){

					$.each(data.Partner_Type, function(i, item) {
			            $('#partner_type').append($('<option>').text(item.name).attr('value', item.id));
			        });

					$.each(data.Products_Displayed, function(i, item) {
			            $('<input type="radio" name="products_displayed" value="' + item.id + '" /><label> ' + item.name + '</label><br />')
			            	.appendTo($("#products_displayed_block"));

			        });

			        $.each(data.Audience_Type, function(i, item) {
			            $('#audience_type').append($('<option>').text(item.name).attr('value', item.id));
			        });

			        $.each(data.Marketing_Items, function(i, item) {
			            $('<input type="radio" name="products_displayed" value="' + item.id + '" /><label> ' + item.name + '</label><br />')
			            	.appendTo($("#markenting_items_block"));
			        });

			        $.each(data.Demo_Equipment, function(i, item) {
			            $('<input type="radio" name="products_displayed" value="' + item.id + '" /><label> ' + item.name + '</label><br />')
			            	.appendTo($("#demo_equipment_block"));
			        });
				}
			});
		},

		sendCollateral: function(){
			var that = this;
			$(document).on('submit','#docContainer',function( event ) {
				event.preventDefault();

				$('body').append('<div class="popup-img full-height"><span class="close-me">X</span><img src="./rw_common/themes/tesla_pro/images/loader.gif"></img></div>');

				var $submitButton = $(this).find("input[type=submit]");

				$submitButton.prop("disabled", true);

				//clean the errors
				$('#docContainer input,#docContainer select,#docContainer textarea').removeClass('glowing-border');
				$('#docContainer .error-msg').text('');

				var data = $('#docContainer').serializeArray().reduce(function(obj, item) {
					obj[item.name] = item.value;

				    return obj;
				}, {});

				$.ajax({
					method: "POST",
					type: "POST",
					url: "./backoffice/public/api/send-collateral",
					data: data,
					dataType: 'json',
					success: function(dataSuccess,status,xhr){
						if (dataSuccess.status && dataSuccess.status == 200)
						{
							// We send a nice popup alert telling the user the information was uploaded
							that.showMessage('Thank You For Your Submission');
							$('#myNotificationsPopup').fadeOut(3000, function(){
								$.modal.close();
							});

							$submitButton.prop("disabled", false);

						}
					},
					error: function( data ){
						var errors = JSON.parse(data.responseText);

				        $.each(errors, function(i, item) {
				        	$('#docContainer input[name="' + i + '"]').addClass('glowing-border');
				         	$('#docContainer input[name="' + i + '"] ~ span.error-msg:first').text(item[0]);

				         	$('#docContainer textarea[name="' + i + '"]').addClass('glowing-border');
				         	$('#docContainer textarea[name="' + i + '"] ~ span:first').text(item[0]);
				        });

						$('body div.popup-img').remove();

						$submitButton.prop("disabled", false);
					}
				});


			});
		}
	};

	(function(){
		MAIN.init();
		$( document ).ready(function() {
		    // $('.datepicker').datepicker();
			$('.datepicker').datetimepicker();
			$('input:checkbox[value=All]').change( function(){
				 $("input:checkbox[name="+this.name+"][value!=Other]").prop('checked',this.checked);
				 $("input:checkbox[name="+this.name+"][value='No Items Needed']").prop('checked', false);
			});

			$("input:checkbox[value='No Items Needed']").change( function(){
				  $("input:checkbox[name="+this.name+"][value!='No Items Needed']").prop('checked', false);
			});

			$("input:checkbox[value!='No Items Needed']").change( function(){
				  $("input:checkbox[name="+this.name+"][value='No Items Needed']").prop('checked', false);
			});

			// now for accesoiries
			$("input:checkbox[value='None Needed']").change( function(){
				  $("input:checkbox[name="+this.name+"][value!='None Needed']").prop('checked', false);
			});

			$("input:checkbox[value!='None Needed']").change( function(){
				  $("input:checkbox[name="+this.name+"][value='None Needed']").prop('checked', false);
			});

			$("#myModal").on('shown.bs.modal', function () {
		        $(".modal").css('display', 'block');
		    });

		    $("#country").change(function(){
		    	if(this.value  != 'United States of America')
		    	{
		    		$("#state").hide();
		    		$(".state_label").hide();

					$("#province").show();
					$("#province_label").show();

		    	}

		    	else
		    	{
					$("#province").hide();
					$("#province_label").hide();
		    		$("#state").show();
		    		$(".state_label").show();
		    	}
		    });

		    $(document).on('change', '#country', function () {
		        if(this.value  != 'United States of America')
		    	{
		    		$("#state").hide();
		    		$("#state_label").hide();

		    	}

		    	else
		    	{
		    		$("#state").show();
		    		$("#state_label").show();
		    	}
		    });

		    $(document).on("click", "#signage-items", function(event) {
		    	$this = $(this);
		    	if ($this.is(":checked")) {
		    		var disableMe = false;
		    	} else {
		    		var disableMe = true;
		    		$("input[type=checkbox][name=additional_items_signage]").prop("checked", false);
		    	}
		    	$("input[type=checkbox][name=additional_items_signage]").prop("disabled", disableMe);
		    });
		});
	})();

})(jQuery);

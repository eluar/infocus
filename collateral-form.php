<?php
session_start();

if (!$_SESSION['is_member_user']) {
    header('Location: ./login.php?refer_to=collateral-form.php');
}
?>

<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Start of the headers for CoffeeCup Web Form Builder -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="icon" type="image/vnd.microsoft.icon" href="rw_common/images/favicon.ico">

    <link rel="stylesheet" href="rw_common/themes/tesla_pro/css/foundation.css">
    <link rel="stylesheet" href="rw_common/themes/tesla_pro/css/app.css">

    <link rel="stylesheet" type="text/css" media="all" href="rw_common/themes/tesla_pro/css/main.css" />
    <link rel="stylesheet" type="text/css" media="all" href="rw_common/themes/tesla_pro/css/jquery-ui-1.8.5.custom.css" />


    <!-- End of the headers for CoffeeCup Web Form Builder -->
    <title>Infocus Collateral Form</title>

    <script type="text/javascript"  src="rw_common/themes/tesla_pro/js/jquery.min.js"></script>
    <script type="text/javascript" src="rw_common/themes/tesla_pro/js/libs_js/jquery-ui-1.8.9.custom.min.js"></script>
    <script type="text/javascript" src="rw_common/themes/tesla_pro/js/libs_js/jquery.ui.datepicker.js"></script>
    <script type="text/javascript" src="rw_common/themes/tesla_pro/js/lang/messages_datepicker.js"></script>
    <script src="rw_common/themes/tesla_pro/js/libs_js/jquery.mlens-1.6.min.js" type="text/javascript"></script>

    <style media="screen">

        body {
            background-color: rgb(246,246,246);
        }

        form .row .columns {
            float: left;
        }

        .column:last-child:not(:first-child), .columns:last-child:not(:first-child) {
            float: left;
        }

        form .row .columns textarea {
            max-width: 100%;
            min-width: 220px;
        }

        .message-success {
            height: 100%;
            width: 100%;
            margin: 0 auto;
            position: fixed;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: rgba(0,0,0,0.75);
            text-align: center;
        }

        .message-success span {
            background-color: #fff;
            top: 47%;
            padding: 2em;
            font-size: 24px;
            color: #68467D;
            position: fixed;
            left: 30%;
            width: 40%;
        }

        .popup-img {
            width: 100%;
            height: 600%;
            position: absolute;
            top: 0;
            left: 0;
            background-color: rgba(0,0,0,0.75);
        }

        .popup-img img {
            position: fixed;
            top: 5em;
            margin-left: 25%;
            border: solid 3px #000;
            box-shadow: 10px 10px 5px 0px rgba(0,0,0,0.75);
            max-height: 90%;
        }

        .popup-img.full-height {
            height: 500%;
            z-index: 10;
        }

        .popup-img.full-height img {
            position: fixed;
            top: 25%;
            left: 20%;
            max-height: 90%;
            width: 40%;
            border: 0;
            box-shadow: none;
        }

        form .row img {
            cursor: pointer;
        }
    </style>

  </head>

  <body>
    <div class="row" style="width: 100%; margin-left:10em;">
      <form id="docContainer" enctype="multipart/form-data" method="POST" action="./" novalidate="novalidate" class="" data-form="publish">

      <div class="row">
        <div class="small-12 medium-12 text-center columns" style="margin-bottom:20px;">
            <h2>Collateral Marketing Request Form</h2>
        </div>
      </div>

      <?php include('./rw_common/themes/tesla_pro/partials/collateral-form-common.php'); ?>

      <div id="fb-submit-button-div" class="fb-item-alignment-left fb-footer" style="min-height:0px;">
        <input type="submit" class="button radius" id="fb-submit-button" data-regular="" value="Submit" />
      </div>
      <p>
          <a href="./"> Go to MDF </a> <br />
          <a href="#" id="logout"> Logout </a>
      </p>

      </form>
    </div>
    <script type="text/javascript">
    $(document).ready(function()
    {
      $('.datepicker').datepicker(); console.log('render');
      console.log("now shoot!");
      $("img.zoom").mlens(
      {
          imgSrc: $("img.zoom").attr("data-big"),	  // path of the hi-res version of the image
        //   imgSrc2x: $("#gear").attr("data-big2x"),  // path of the hi-res @2x version of the image
                                                    //for retina displays (optional)
          lensShape: "square",                // shape of the lens (circle/square)
          lensSize: ["100%","100%"],            // lens dimensions (in px or in % with respect to image dimensions)
                                              // can be different for X and Y dimension
          borderSize: 2,                  // size of the lens border (in px)
          borderColor: "#000",            // color of the lens border (#hex)
          borderRadius: 0,                // border radius (optional, only if the shape is square)
        //   imgOverlay: $("#gear").attr("data-overlay"), // path of the overlay image (optional)
          overlayAdapt: true,    // true if the overlay image has to adapt to the lens size (boolean)
          zoomLevel: 1,          // zoom level multiplicator (number)
          responsive: true       // true if mlens has to be responsive (boolean)
      });

      $(document).on('submit','#docContainer',function( event ) {
          event.preventDefault();

          var $submitButton = $(this).find("input[type=submit]");
          $submitButton.prop("disabled", true);

          $('body').append('<div class="popup-img full-height"><span class="close-me">X</span><img src="./rw_common/themes/tesla_pro/images/loader.gif"></img></div>');
          $('body div.popup-img').css("height", "550%");
          $('body div.popup-img img').css("left", "10%");

          //clean the errors
          $('#docContainer input,#docContainer select,#docContainer textarea').removeClass('glowing-border');
          $('#docContainer .error-msg').text('');

          var data = $('#docContainer').serializeArray().reduce(function(obj, item) {
              obj[item.name] = item.value;

              return obj;
          }, {});

          $.ajax({
              method: "POST",
              type: "POST",
              url: "./backoffice/public/api/send-collateral",
              data: data,
              dataType: 'json',
              success: function(dataSuccess,status,xhr){
                  if (dataSuccess.status && dataSuccess.status == 200)
                  {
                      // We send a nice popup alert telling the user the information was uploaded
                      $('body div.popup-img').css("background-color", "#fff").css("top", "15%");
          			  $('body div.popup-img img').prop('src', './rw_common/themes/tesla_pro/images/subgraphic.png');
                      window.setTimeout(function () {
                          window.location.reload();
                      }, 3000);

                  }
              },
              error: function( data ){
                  var errors = JSON.parse(data.responseText);

                  $.each(errors, function(i, item) {
                      $('#docContainer input[name="' + i + '"]').addClass('glowing-border');
                      $('#docContainer input[name="' + i + '"] ~ span.error-msg:first').text(item[0]);

                      $('#docContainer textarea[name="' + i + '"]').addClass('glowing-border');
                      $('#docContainer textarea[name="' + i + '"] ~ span:first').text(item[0]);
                  });

                  $('body div.popup-img').remove();

                  $submitButton.prop("disabled", false);
              }
          });


      });

      $(document).on('click','#logout',function( event ) {
          event.preventDefault();
          $.ajax({
              method: "GET",
              url: "./backoffice/public/api/form-logout",
              dataType: 'json',
              success: function(response,status,xhr) {
                  document.location.reload();
              },
              error: function( data ) {
                  console.log(data);
              }
          });
      });

      $(document).on('click', 'img.lets-do-this',	function(event) {
          $('#docContainer').append('<div class="popup-img"><img src="'+$(this).data("big")+'"></img></div>');
      });

      $(document).on('click', 'div.popup-img',	function(event) {
          $(this).remove();
      });

    });
    </script>
  </body>
</html>

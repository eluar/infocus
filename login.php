<?php
    session_start();
    if ($_SESSION['is_member_user']) {
		header('Location: ./' . (isset($_GET['refer_to']) ? $_GET['refer_to'] : '' ) );
	}
 ?>

<!doctype html>
<html lang="en" class="no-js">

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="icon" type="image/vnd.microsoft.icon" href="rw_common/images/favicon.ico">

	<title>Infocus - Backoffices</title>

	<!-- Font awesome -->
	<link rel="stylesheet" href="backoffice/public/css/font-awesome.min.css">
	<!-- Sandstone Bootstrap CSS -->
	<link rel="stylesheet" href="backoffice/public/css/bootstrap.min.css">
	<!-- Bootstrap Datatables -->
	<link rel="stylesheet" href="backoffice/public/css/dataTables.bootstrap.min.css">
	<!-- Bootstrap social button library -->
	<link rel="stylesheet" href="backoffice/public/css/bootstrap-social.css">
	<!-- Bootstrap select -->
	<link rel="stylesheet" href="backoffice/public/css/bootstrap-select.css">
	<!-- Bootstrap file input -->
	<link rel="stylesheet" href="backoffice/public/css/fileinput.min.css">
	<!-- Awesome Bootstrap checkbox -->
	<link rel="stylesheet" href="backoffice/public/css/awesome-bootstrap-checkbox.css">
	<!-- Admin Stye -->
	<link rel="stylesheet" href="backoffice/public/css/style.css">

	<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

	<div class="login-page bk-img" style="background-image: url(backoffice/public/img/banner_7.jpeg);">
		<div class="form-content">
			<div class="container">
				<div class="row">
					<div class="col-md-6 col-md-offset-3">
						<h1 class="text-center text-bold mt-4x">Sign in</h1>
						<div class="well row pt-2x pb-3x bk-light">
							<div class="col-md-8 col-md-offset-2">
								<form action="" method="post" class="mt" id="loginForm">

									<label for="" class="text-uppercase text-sm">Your Username</label>
									<input type="text" placeholder="Username" name="email" class="form-control mb">

									<label for="" class="text-uppercase text-sm">Password</label>
									<input type="password" placeholder="Password" name="password" class="form-control mb">

									<div class="checkbox checkbox-circle checkbox-info">
										<input id="checkbox7" type="checkbox" checked>
                                        <?php /** not right now **/ ?>
										<?php /**
                                        <label for="checkbox7">
											Keep me signed in
										</label>
                                         **/ ?>
									</div>

									<button class="btn btn-primary btn-block" type="submit">LOGIN</button>

								</form>
							</div>
						</div>
						<div class="error">
							<div id="loginErrorDiv" style="display:none;" class="alert alert-dismissible alert-danger">
							    <strong>Error</strong>
						    </div>
						</div>
						<div class="text-center text-light">
                            <?php /** not right now **/ ?>
							<?php /** <a href="#" class="text-light">Forgot password?</a> **/ ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Loading Scripts -->
	<script src="backoffice/public/js/jquery.min.js"></script>
	<script src="backoffice/public/js/bootstrap-select.min.js"></script>
	<script src="backoffice/public/js/bootstrap.min.js"></script>
	<script src="backoffice/public/js/jquery.dataTables.min.js"></script>
	<script src="backoffice/public/js/dataTables.bootstrap.min.js"></script>
	<script src="backoffice/public/js/Chart.min.js"></script>
	<script src="backoffice/public/js/fileinput.js"></script>
	<script src="backoffice/public/js/chartData.js"></script>
	<script src="backoffice/public/js/main.js"></script>

	<script>
        $(document).ready(function() {
            $messageElement = $("#loginErrorDiv");
            $("#loginForm").on("submit", function(event) {
                event.preventDefault();
                $messageElement.hide();
                $.ajax({
					method: "POST",
					type: "POST",
					url: "./backoffice/public/api/form-login",
					data: $('#loginForm').serialize(),
					dataType: 'json',
					success: function(response,status,xhr){
						if (response.success) {
							document.location.reload();
						} else {
                            console.log(response, $messageElement);
                            $messageElement.find("strong").html(response.message);
                            $messageElement.show();
                        }
					},
					error: function( data ){
						console.log(data);
					}
				});
            });
        });
	</script>

</body>

</html>

<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['middleware' => ['api'], 'prefix'=>'api', "namespace" => "API"], function () {
    Route::post('save-infocus-event/', 'InfocusEventsController@saveEvent');
    Route::get('get-form-data/', 'FormDataController@getFormData');
    Route::get('get-registrations-chart-data/', 'ChartDataController@getRegistrationsChartData');
    Route::post('form-login', 'UserController@formLogin');
    Route::get('form-logout', 'UserController@formLogout');
    Route::post('send-collateral', 'InfocusEventsController@sendCollateral');
    Route::get('collateral', 'CollateralFormController@index');
    Route::post('collateral/sort', 'CollateralFormController@sort');
});

Route::group(["middleware" => ["web"], "namespace" => "Web"], function () {
    Route::match(['get','post'],'login/', 'UserController@login');
    Route::get('/logout', 'UserController@logout');
    Route::get('/', 'DashboardController@index')->middleware('auth');
    Route::get('/collateral', 'CollateralController@index')->middleware('auth');
    Route::post('/collateral/store', 'CollateralController@store')->middleware('auth');
    Route::get('/collateral/new', 'CollateralController@create')->middleware('auth');
    Route::get('/collateral/{collateralItem}', 'CollateralController@show')->middleware('auth');
    Route::patch('/collateral/{collateralItem}', 'CollateralController@update')->middleware('auth');
    Route::delete('/collateral/{collateralItem}', 'CollateralController@destroy')->middleware('auth');
    Route::match(['get','post'],'/export', 'DashboardController@export')->middleware('auth');
});

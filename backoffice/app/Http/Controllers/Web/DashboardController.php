<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Event_Request;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Excel;

class DashboardController extends Controller
{

    public function index(Request $request)
    {
        $this->redirectNonAllowedUser();

    	$items_per_page = 15;
    	$total_rows = Event_Request::count();
    	$events = Event_Request::orderBy('updated_at', 'desc')->paginate($items_per_page);
    	$events->setPath('./');
    	$user = Auth::user();
    	$params = array('user' => $user, 'total_events' => $total_rows, 'events' => $events);
    	return view('backoffice.dashboard', $params);
    }

    public function export(Request $request)
    {
        $this->redirectNonAllowedUser();

        $status = null;
        $message = null;

    	if ($request->isMethod('post')) {
    		$dates = $request->all();
    		$dates['start_Date'] = date("Y-m-d", strtotime($dates['start_Date'])) . ' 00:00:00';
    		$dates['end_Date'] = date("Y-m-d", strtotime($dates['end_Date'])) . ' 23:59:59';

            // we can switch the field to activity_event_date once we ask Sami
            $events = Event_Request::whereBetween('created_at', array_values($dates))
    							->orderBy('activity_event_date', 'desc')
    							->get();

            if ($events->count()) {
                Excel::create('events_request_' . time(), function($excel) use($events) {
    			    $excel->sheet('Sheet 1', function($sheet) use($events) {
    			        $sheet->fromArray($events);
    			    });
    			})->export('xls');
                $status = 'success';
                $message = 'A new XLS report file was created Successfully';
            } else {
                $status = 'warning';
                $message = 'There is nothing to be reported using those dates';
            }

    	}

        return view('backoffice.csv-export', array('user' => Auth::user(), 'status' => $status, 'message' => $message));
    }

    private function redirectNonAllowedUser()
    {
        if (Auth::user() && Auth::user()->role !== 'manager') {
            Auth::logout();
            return redirect()->intended('/login');
        }
    }
}

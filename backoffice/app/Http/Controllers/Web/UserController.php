<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Hash;

class UserController extends Controller
{

    public function login(Request $request)
    {
        if ($request->isMethod('post')) {

            /*$this->validate($request, [
            'username' => 'required|max:255',
            'password' => 'required|max:255',
            ]);*/
            $loginData = $request->all();

            if (Auth::attempt($loginData)) {
                if (Auth::user()->role == 'manager') {
                    return redirect()->intended('/');
                } else {
                    Auth::logout();
                    $request->session()->flash('login-fail', 'You have not permissions to access here');
                }

            } else {
                $request->session()->flash('login-fail', 'The User/password combinaton provided is invalid');
            }
        }

        return view('backoffice.login');
    }

    public function logout()
    {
    	Auth::logout();
    	return redirect()->intended('/login');

    }
}

<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\CollateralItem as Collateral;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\ImageManagerStatic as Image;


class CollateralController extends Controller
{

    public function index(Request $request)
    {
        $this->redirectNonAllowedUser();

    	$total_rows = Collateral::count();
    	$collaterals = Collateral::orderBy('order', 'asc')->get();
    	// $collaterals->setPath('./');
    	$user = Auth::user();
    	$params = array('user' => $user, 'total_collaterals' => $total_rows, 'collaterals' => $collaterals);
    	return view('backoffice.collateral.index', $params);
    }

    /**
     * Show the form for creating a new collateral item.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        return view('backoffice.collateral.create', array('user' => $user));
    }

    /**
     * Store a newly created collateral item in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg',
        ]);

        $imagesNames = $this->processImages($request);

        $item = new Collateral([
          'name' => $request->get('name'),
          'description' => $request->get('description'),
          'image_lg_url' => $imagesNames['imageLgUrl'],
          'image_sm_url' => $imagesNames['imageSmUrl'],
          'active' => $request->has('active'),
        ]);
        $item->save();
        $user = Auth::user();
        return redirect()->intended('/collateral')->with('success','Item successfuly saved');
    }

    /**
     * Show the form for editing the specified collateral item.
     *
     * @param  Collateral $collateralItem
     * @return \Illuminate\Http\Response
     */
    public function show(Collateral $collateralItem)
    {
        $user = Auth::user();
        return view('backoffice.collateral.show', array('item' => $collateralItem, 'user' => $user));
    }

    /**
     * Update the specified resource in collateral item.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Collateral $collateralItem
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Collateral $collateralItem)
    {
        $this->validate($request, [
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg',
        ]);

        $imagesNames = $this->processImages($request, $collateralItem, false);

        $collateralItem->name = $request->get('name');
        $collateralItem->description = $request->get('description');
        $collateralItem->image_lg_url = $imagesNames['imageLgUrl'];
        $collateralItem->image_sm_url = $imagesNames['imageSmUrl'];
        $collateralItem->active = $request->has('active');
        $collateralItem->save();
        return redirect()->intended('/collateral')->with('success','Item successfuly saved');
    }

    /**
     * Remove the specified resource from collateral.
     *
     * @param  Collateral $collateralItem
     * @return \Illuminate\Http\Response
     */
    public function destroy(Collateral $collateralItem)
    {
      $collateralItem->delete();
      return redirect()->intended('/collateral');
    }


    private function redirectNonAllowedUser()
    {
        if (Auth::user() && Auth::user()->role !== 'manager') {
            Auth::logout();
            return redirect()->intended('/login');
        }
    }

    /**
     * it expect $storing to be true for creation and false for update
     * 
     * @param Request $request
     * @param CollateralItem $collateralItem
     * @param bool $creating
     * @return array
     */
    private function processImages(Request $request, Collateral $collateralItem = null)
    {
        $hasImage = $request->hasFile('image');

        if ($hasImage) {
            $image = $request->file('image');
            $imageName = time().'.'.$image->getClientOriginalExtension();
            
            $imageLgUrl = 'lg_' . $imageName;
            $imageSmUrl = 'sm_' . $imageName;

            // seting up destination path
            $destinationPath = public_path('../../rw_common/images');
            
            // deleting current images
            if ($collateralItem && $collateralItem->image_lg_url) {
                unlink($destinationPath . '/lg/' . $collateralItem->image_lg_url);
            }
            if ($collateralItem && $collateralItem->image_sm_url) {
                unlink($destinationPath . '/sm/' . $collateralItem->image_sm_url);
            }

            // copying and resizing image
            $image_resize = Image::make($image->getRealPath());              
            $image_resize->resize(32, 41);

            // saving original and resize images
            $image->move($destinationPath . '/lg', $imageLgUrl);
            $image_resize->save($destinationPath . '/sm/' . $imageSmUrl);
        } else {
            $imageLgUrl = ($collateralItem) ? $collateralItem->image_lg_url : null;
            $imageSmUrl = ($collateralItem) ? $collateralItem->image_sm_url : null;
        }

        return array(
            'imageLgUrl' => $imageLgUrl, 
            'imageSmUrl' => $imageSmUrl,
        );
    }
}

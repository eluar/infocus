<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Event_Request;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class ChartDataController extends Controller
{
    protected $table = 'users';

    public function __construct()
    {

    }

    public function getRegistrationsChartData()
    {

        try {
    		$chartData = Array();
    		$chartData['labels'] = Array();
    		$chartData['datasets'] = Array();
    		$lastWeekDS = Array();
    		$lastWeekDS['label'] = 'Last week';
    		$lastWeekDS['fillColor'] = 'rgba(220,220,220,0.2)';
    		$lastWeekDS['strokeColor'] = 'rgba(220,220,220,1)';
    		$lastWeekDS['pointColor'] = 'rgba(220,220,220,1)';
    		$lastWeekDS['pointStrokeColor'] = '#fff';
    		$lastWeekDS['pointHighlightFill'] = '#fff';
    		$lastWeekDS['pointHighlightStroke'] = 'rgba(220,220,220,1)';
    		$lastWeekDS['data'] = [0,0,0,0,0,0,0];
    		$thisWeekDS = Array();
    		$thisWeekDS['label'] = 'This week';
    		$thisWeekDS['fillColor'] = 'rgba(151,187,205,0.2)';
    		$thisWeekDS['strokeColor'] = 'rgba(151,187,205,1)';
    		$thisWeekDS['pointColor'] = 'rgba(151,187,205,1)';
    		$thisWeekDS['pointStrokeColor'] = '#fff';
    		$thisWeekDS['pointHighlightFill'] = '#fff';
    		$thisWeekDS['pointHighlightStroke'] = 'rgba(151,187,205,1)';
    		$thisWeekDS['data'] = [0,0,0,0,0,0,0];
    		$dbData = DB::select('SELECT COUNT(*) AS registrations, DATEDIFF(created_at,NOW()) AS days_ago, DATEDIFF(created_at,NOW())<-6 AS is_past_week FROM event_request GROUP BY DATEDIFF(created_at,NOW()), DATEDIFF(created_at,NOW())<-6');
    		for( $daysAgo = 6 ; $daysAgo>0 ; $daysAgo -- ){
    			$thisDay = date('l',strtotime("-$daysAgo days"));
    			$chartData['labels'][] = $thisDay;
    		}
    		foreach($dbData as $data){
    			if($data->is_past_week){
    				$lastWeekDS['data'][$data->days_ago+13] = $data->registrations;
    			}else{
    				$thisWeekDS['data'][$data->days_ago+6]= $data->registrations;
    			}
    		}
    		$chartData['labels'][] = date('l');
    		$chartData['datasets'][] = $lastWeekDS;
    		$chartData['datasets'][] = $thisWeekDS;
            return response()->json($chartData);
        } catch(Exception $e) {
           // do task when error
           //echo $e->getMessage();   // insert query
        }
    }
}

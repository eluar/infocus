<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\CollateralItem as Collateral;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CollateralFormController extends Controller
{

    public function index(Request $request)
    {
        $collaterals = Collateral::where('active', 1)->orderBy('order', 'asc')->get();
    	return $collaterals;
    }

    public function sort(Request $request)
    {
        $sortArray = $request->input('order');

        foreach ($sortArray as $order => $id) {
            Collateral::find($id)->update(['order' => $order + 1]);
        }

        return response()
              ->json(['success' => true]);
    }
}

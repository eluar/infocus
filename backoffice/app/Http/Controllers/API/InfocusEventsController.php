<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Event_Request;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Mail;

class InfocusEventsController extends Controller
{
    protected $table = 'event_request';

    public function __construct()
    {

    }

    /**
    * Delete this:
    *
    * Event Location City * -> event_location_city
    * Event Location State * -> event_location_state
    * Attending InFocus Employees -> attending_infocus_employees
    * --------------------------------
    *
    * Add the following -
    *
    * Delivery address -> delivery_address
    * Company name -> company_name
    * Street address -> street_address
    * City, -> city
    * State (for state keep the drop down we already have), -> state
    * Zip -> zipcode
    * Contact name -> contact_name
    * Contact email -> contact_email
    * Phone number -> phone_number
    * Date needed:  (add calendar) -> date_needed
    * Date can be returned: (Add Calendar) -> date_to_return
    */
    public function saveEvent(Request $request)
    {
        $event_request_data = $request->all();
        $validation_rules = [
            'your_name'                 => 'required|max:255',
            'email'                     => 'required|email|max:255',
            'partner_name'              => 'required|max:255',
            'program_name'              => 'required|max:255',
            'cost_center'               => 'required|max:150',
            'delivery_address'          => 'required|max:250',
            'company_name'              => 'required|max:100',
            'street_address'            => 'required|max:150',
            'city'                      => 'required|max:50',
            'province'                  => 'max:50',
            'country'                   => 'required|max:70',
            'zipcode'                   => 'required|string|max:9',
            'contact_name'              => 'required|max:150',
            'contact_email'             => 'max:150|email',
            'phone_number'              => 'max:16',
            // 'date_needed'            => 'required|date|after:+2 weeks',
            'date_needed'               => 'required|date',
            // 'date_to_return'         => 'required|date|after:+2 weeks',
            'date_to_return'            => 'required|date',

            'description_of_event'      => 'required|max:255',
            // 'activity_event_date'    => 'required|date|max:255|after:+2 weeks',
            'activity_event_date'       => 'required|date|max:255',
            // 'activity_event_date_to' => 'required|date|max:255|after:+2 weeks',
            'activity_event_date_to'    => 'required|date|max:255',
            'url_for_event'             => 'max:100|url',
            //'product_displayed'       => 'max:255',
            'audience_type'             => 'string|max:255',
            'marketing_item'            => 'required|max:655',
            'additional_items'          => 'array',
            'additional_items_signage'  => 'array',
            'demo_equipment'            => 'required|array|max:355',
            'accessories'               => 'required|array|max:355',
            // 'email_blast_salesforce' => 'required|max:255',
            // 'event_email_campaign'   => 'required|in:Yes,No'
        ];

        if($event_request_data['country'] == 'United States of America')
        {
            $validation_rules['state'] = 'required|max:50';
        }
        else
        {
            unset($event_request_data['state']);
        }

        $this->validate($request, $validation_rules);


        foreach (['demo_equipment', 'product_displayed', 'accessories'] as $key => $value) {
            if (!array_key_exists($value, $event_request_data)) {
                $event_request_data[$value] = [];
            }            
        } 

        $additionalItems = isset($event_request_data['additional_items_signage']) ? $event_request_data['additional_items_signage'] : [];
        unset($event_request_data['additional_items_signage']);

        if (isset($event_request_data['additional_items'])) {

            if (in_array('Signage', $event_request_data['additional_items']) && count($additionalItems) == 0) {
                // $this->error('additional_items', '');
                // abort(422, ['additional_items' => 'Signage should have some item attached to it.']);
            } else {
                $event_request_data['additional_items'][]  = implode(', ', $additionalItems);
            }

        } else {
            $event_request_data['additional_items'] = [];
        }

        $event_request_data['product_displayed'] = str_replace('All,', '', implode(',', $event_request_data['product_displayed']));
        $event_request_data['marketing_item']    = str_replace('All,', '', implode(',', $event_request_data['marketing_item']));
        $event_request_data['additional_items']  = str_replace('All,', '', implode(',', $event_request_data['additional_items']));
        
        $event_request_data['demo_equipment']    = str_replace('All,', '', implode(',', $event_request_data['demo_equipment']));
        $event_request_data['accessories']       = str_replace('All,', '', implode(',', $event_request_data['accessories']));
        
        $event_request_data['activity_event_date'] = date("Y-m-d H:i:s", strtotime($event_request_data['activity_event_date']));
        $event_request_data['activity_event_date_to'] = date("Y-m-d H:i:s", strtotime($event_request_data['activity_event_date_to']));

        $event_request_data['date_needed'] = date("Y-m-d H:i:s", strtotime($event_request_data['date_needed']));
        $event_request_data['date_to_return'] = date("Y-m-d H:i:s", strtotime($event_request_data['date_to_return']));

        $eventModel = new Event_Request();

        foreach ($event_request_data as $event_key => $event_value) {
            if ($event_key != 'form_token' && $event_value != 'All') {
                $eventModel->$event_key = $event_value;
            }
        }

        try{
            $eventModel->save();
            if(!empty($eventModel->id) && $eventModel->id > 0)
            {
                Mail::send('emails.new-event', ['eventModel' => $eventModel], function($message) use ($eventModel)
                {
                    $toEmailList = [
                        // 'luar007@gmail.com',
                        'sandra.daniels@infocus.com',
                    ];
                    if ($eventModel->email_blast_salesforce == 'Yes') {
                        $toEmailList = array_merge($toEmailList, ['Melissa.Adamson@infocus.com', 'Anshu.Ahluwalia@infocus.com']);
                    }
                    $message->to($toEmailList)->subject('New Event Request'); // TEST
                });
                if ($request->has('email')) {
                    Mail::send('emails.new-event-client', ['eventModel' => $eventModel], function($message) use ($event_request_data)
                    {
                        $message->to($event_request_data['email'])->subject('You submitedd a New Event Request'); // PROD
                    });
                }
                return response()->json([
                    'event_status_code' => 200,
                    'message' => 'Thank you for submitting!'
                ]);
            }
            else
            {
                return response()->json([
                    'event_status_code' => 500,
                    'message' => 'Something wrong occurred'
                ]);
            }
        }
        catch(Exception $e){
           // do task when error
           return response()->json([
               'event_status_code' => 500,
               'message' => $e->getMessage(),
           ]);
        }
    }

    public function sendCollateral(Request $request)
    {

        $validation_rules = [
            'date_needed' => 'required|max:255',
            'event_name' => 'required|max:120',
            'requested_by' => 'required|max:255',
            'cost_center' => 'required|max:150',
            'company' => 'required|max:255',
            'email' => 'required|email|max:255',
            'attn' => 'required|max:255',
            'street' => 'required|max:255',
            'city' => 'required|max:255',
            'province' => 'max:50',
            'country' => 'required|max:250',
            'zip' => 'required|max:255',
            'phone' => 'required|max:255'
        ];

        $data = $request->all();
        if($data['country'] == 'United States of America')
        {
            $validation_rules['state'] = 'required|max:50';
        }
        else
        {
            unset($data['state']);
        }

        $this->validate($request, $validation_rules);



        $viewData = $this->build_info_for_view($data);

        Mail::send('emails.collateral', $viewData, function($message)
        {
            $message->from('noreply@bridgeheadstudios.com', 'InFocus Lit Request');
            $message->to(config('mail.mailList'))->subject('Collateral Marketing Request');
        });

        if ($request->has('email')) {
            Mail::send('emails.collateral-client', $viewData, function($message) use ($request)
            {
                $message->to($request->get('email'))->subject('You submited a New Collateral Marketing Request');
            });
        }

        return response()->json([
            'status' => 200,
            'message' => 'Mail was sent'
            ]);
    }

    private function build_info_for_view($formData)
    {
        $returnData = array();
        foreach ($formData as $key => $value) {
            if(strpos($key, 'qty_') !== false || strpos($key, 'notes_') !== false)
            {
                $exp = explode("_", $key);
                $rest = array_slice($exp, 1);
                $real_key = implode("_", $rest);
                if($exp[0] == 'qty')
                {
                    $returnData['grid'][$real_key]['label'] = str_replace("_", " ", $real_key);
                    $returnData['grid'][$real_key]['qty'] = $value;
                }

                if($exp[0] == 'notes')
                {
                    $returnData['grid'][$real_key]['notes'] = $value;
                }
            }
            else
            {
                $returnData['flat'][$key]['label'] = str_replace("_", " ", $key);
                $returnData['flat'][$key]['value'] = $value;
            }
        }

        return $returnData;

    }
}

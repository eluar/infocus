<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Audience_Type;
use App\Models\Demo_Equipment;
use App\Models\Marketing_Items;
use App\Models\Partner_Type;
use App\Models\Products_Displayed;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class FormDataController extends Controller
{
    //
    public function __construct()
    {

    }

    public function getFormData()
    {
    	return response()->json([
		    'Audience_Type' => Audience_Type::all(),
		    'Demo_Equipment' => Demo_Equipment::all(),
		    'Marketing_Items' => Marketing_Items::all(),
		    'Marketing_Items' => Marketing_Items::all(),
		    'Partner_Type' => Partner_Type::all(),
		    'Products_Displayed' => Products_Displayed::all(),
		]);
    }
}

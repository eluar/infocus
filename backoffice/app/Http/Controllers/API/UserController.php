<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Hash;

class UserController extends Controller
{

    public function formLogin(Request $request)
    {
        $loginData = $request->all();

        if (Auth::attempt($loginData)) {
            if (Auth::user()->role == 'manager' || Auth::user()->role == 'member') {
                $arReturn = [ 'success' => true, 'message' => 'You\'ve been signed in successfully'];
                session_start();
                $_SESSION['is_member_user'] = true;
            } else {
                Auth::logout();
                $arReturn = [ 'success' => false, 'message' => 'You have not permissions to access here'];
            }
        } else {
            $arReturn = [ 'success' => false, 'message' => 'The User/password combinaton provided is invalid'];
        }

        return response()->json($arReturn);
    }

    public function formLogout()
    {
        session_start();
        unset($_SESSION['is_member_user']);
    	Auth::logout();
        return ['success' => true, 'message' => 'Successfully signned out'];
    }
}

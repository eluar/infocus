<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CollateralItem extends Model
{
    protected $fillable = ['name', 'description', 'image_lg_url', 'image_sm_url', 'active', 'order'];

    protected $table = 'collateral_requests';

    protected $appends = ['full_lg_url', 'full_sm_url'];

    private $destinationPath = '/rw_common/images';

    public function getFullLgUrlAttribute()
    {
    	return $this->destinationPath . '/lg/' . $this->image_lg_url;
    }

    public function getFullSmUrlAttribute()
    {
    	return $this->destinationPath . '/sm/' . $this->image_sm_url;
    }

}
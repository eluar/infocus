<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EventEmailCampaignField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('ALTER TABLE  `event_request` ADD  `event_email_campaign` VARCHAR(3) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL AFTER  `email_blast_salesforce`;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('ALTER TABLE `event_request` DROP `event_email_campaign`;');
        //ALTER TABLE `event_request` DROP `event_email_campaign`;
    }
}

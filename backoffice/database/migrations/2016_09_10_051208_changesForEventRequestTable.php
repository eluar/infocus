<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangesForEventRequestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('event_request', function ($table) {
            $table->dropColumn('partner_type');
            $table->dropColumn('requested_amount');
            $table->date('activity_event_date_to')->after('activity_event_date');
            // $table->string('url_for_event', 100)->nullable()->change();
        });

        DB::statement('ALTER TABLE `event_request` MODIFY `url_for_event` INTEGER UNSIGNED NULL;');
        DB::statement('UPDATE `event_request` SET `url_for_event` = NULL WHERE `url_for_event` = "";');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('event_request', function (Blueprint $table) {
            $table->string('partner_type', 100)->after('email');
            $table->string('requested_amount', 100)->after('event_location_state');
            $table->dropColumn('activity_event_date_to');
            // $table->string('url_for_event', 100)->change();
        });

        DB::statement('UPDATE `event_request` SET `url_for_event` = "" WHERE `url_for_event` IS NULL;');
        DB::statement('ALTER TABLE `event_request` MODIFY `url_for_event` INTEGER UNSIGNED NOT NULL;');
    }
}

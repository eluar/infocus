<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddingAdditionalItemsToEventRequest extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('event_request', function (Blueprint $table) {
            $table->string('additional_items', 350)->after('marketing_item')->nullable(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('event_request', function ($table) {
            $table->dropColumn('additional_items');
        });
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventRequest extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_request', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('your_name', 100);
            $table->string('email', 100);
            $table->string('partner_type', 100);
            $table->string('partner_name', 200);
            $table->string('program_name', 200);
            $table->string('event_location_city', 100);
            $table->string('event_location_state', 100);
            $table->string('requested_amount', 100);
            $table->string('description_of_event', 100);
            $table->date('activity_event_date');
            $table->string('product_displayed', 350);
            $table->string('url_for_event', 100);
            $table->string('audience_type', 100);
            $table->string('marketing_item', 350);
            $table->string('demo_equipment', 350);
            $table->string('email_blast_salesforce', 100);
            $table->text('attending_infocus_employees');
            $table->text('additional_comments');
            $table->text('comment');
            $table->timestamps();
        });

        /*
        // Uncomment this code to replace the code up to use ids instead of strings
        // 
        Schema::create('event_request', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('your_name', 100);
            $table->string('email', 100);
            $table->integer('partner_type_id')->unsigned()->nullable();
            $table->string('partner_name', 200);
            $table->string('program_name', 200);
            $table->string('event_location_city', 100);
            $table->string('event_location_state', 100);
            $table->text('attending_infocus_employees');
            $table->string('requested_amount', 100);
            $table->string('description_of_event', 100);
            $table->string('activity_event_date', 100);
            $table->integer('product_displayed_id')->unsigned()->nullable();
            $table->string('url_for_event', 100);
            $table->integer('audience_type_id')->unsigned()->nullable();
            $table->integer('marketing_item_id')->unsigned()->nullable();
            $table->integer('demo_equipment_id')->unsigned()->nullable();
            $table->integer('email_blast_salesforce');
            $table->text('additional_comments');
            $table->text('comment');

        });

        Schema::table('event_request', function (Blueprint $table) {
            $table->foreign('partner_type_id')->references('id')->on('partner_type');
            $table->foreign('product_displayed_id')->references('id')->on('products_displayed');
            $table->foreign('audience_type_id')->references('id')->on('audience_type');
            $table->foreign('marketing_item_id')->references('id')->on('marketing_items');
            $table->foreign('demo_equipment_id')->references('id')->on('demo_equipment');
        });*/
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        /*Schema::table('event_request', function (Blueprint $table) {
            $table->dropForeign('posts_user_id_foreign');
            $table->dropForeign('posts_user_id_foreign');
            $table->dropForeign('posts_user_id_foreign');
        });*/
        Schema::drop('event_request');
    }
}

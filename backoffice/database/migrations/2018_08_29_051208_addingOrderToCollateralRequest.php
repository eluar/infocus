<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\CollateralItem as Collateral;

// 2018_08_29_051208_addingOrderToCollateralRequest

class AddingOrderToCollateralRequest extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('collateral_requests', function (Blueprint $table) {
            $table->integer('order')->after('image_sm_url')->nullable()->default(0);
        });

        foreach (Collateral::all() as $index => $collateral) {
            $collateral->order = $index;
            $collateral->save();
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('collateral_requests', function ($table) {
            $table->dropColumn('order');
        });
    }
}
<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddingProvinceAndCostCenter extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('event_request', function ($table) {
            $table->string('province', 50)->after('street_address')->nullable(true);
            $table->string('cost_center', 150)->after('program_name')->nullable(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('event_request', function ($table) {
            $table->dropColumn('province');
            $table->dropColumn('cost_center');
        });
    }
}

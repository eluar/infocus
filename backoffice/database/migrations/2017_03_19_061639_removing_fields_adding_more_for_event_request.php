<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemovingFieldsAddingMoreForEventRequest extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Delete this:
        //
        // Event Location City * -> event_location_city
        // Event Location State * -> event_location_state
        // Attending InFocus Employees -> attending_infocus_employees
        // --------------------------------
        //
        // Add the following -
        //
        // Delivery address -> delivery_address
        // Company name -> company_name
        // Street address -> street_address
        // City, -> city
        // State (for state keep the drop down we already have), -> state
        // Zip -> zipcode
        // Contact name -> contact_name
        // Contact email -> contact_email
        // Phone number -> phone_number
        // Date needed:  (add calendar) -> date_needed
        // Date can be returned: (Add Calendar) -> date_to_return

        Schema::table('event_request', function ($table) {
            $table->string('delivery_address', 250)->after('attending_infocus_employees')->nullable(false);
            $table->string('company_name', 100)->after('delivery_address')->nullable(false);
            $table->string('street_address', 150)->after('company_name')->nullable();
            $table->string('city', 50)->after('street_address')->nullable(false);
            $table->string('state', 50)->after('city')->nullable(false);

            $table->mediumInteger('zipcode', false)->unsigned()->after('state')->nullable(false);
            $table->string('contact_name', 150)->after('zipcode')->nullable(false);
            $table->string('contact_email', 150)->after('contact_name')->nullable();
            $table->string('phone_number', 16)->after('contact_email')->nullable();

            $table->date('date_needed', 50)->after('phone_number')->nullable(false);
            $table->date('date_to_return', 50)->after('date_needed')->nullable();
        });

        DB::statement('ALTER TABLE `event_request` MODIFY `event_location_city` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL;');
        DB::statement('ALTER TABLE `event_request` MODIFY `event_location_state` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL;');
        DB::statement('ALTER TABLE `event_request` MODIFY `attending_infocus_employees` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL;');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('ALTER TABLE `event_request` MODIFY `event_location_city` varchar(100) COLLATE utf8_unicode_ci NOT NULL;');
        DB::statement('ALTER TABLE `event_request` MODIFY `event_location_state` varchar(100) COLLATE utf8_unicode_ci NOT NULL;');
        DB::statement('ALTER TABLE `event_request` MODIFY `attending_infocus_employees` varchar(100) COLLATE utf8_unicode_ci NOT NULL;');

        Schema::table('event_request', function ($table) {
            $table->dropColumn('delivery_address');
            $table->dropColumn('company_name');
            $table->dropColumn('street_address');
            $table->dropColumn('city');
            $table->dropColumn('state');
            $table->dropColumn('zipcode');
            $table->dropColumn('contact_name');
            $table->dropColumn('contact_email');
            $table->dropColumn('phone_number');
            $table->dropColumn('date_needed');
            $table->dropColumn('date_to_return');
        });
    }
}

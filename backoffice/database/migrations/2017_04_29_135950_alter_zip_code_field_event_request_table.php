<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterZipCodeFieldEventRequestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('event_request', function (Blueprint $table) {
            $table->string('zipcode')->change();
            $table->string('country', 70)->after('state')->nullable(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('event_request', function ($table) {
            $table->dropColumn('country');
        });
    }
}

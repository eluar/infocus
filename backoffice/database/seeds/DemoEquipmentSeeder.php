<?php

use Illuminate\Database\Seeder;

class DemoEquipmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $equipments = array('JTouch', 'Mondopad', 'Projector', 'DigiEasel', 'CRS 4K', 'Catalyst 4K',
        				'Fusion Catalyst 4500', 'Canvas Touch', 'ConX Exec', 'PixelNet');
        
    	foreach ($equipments as $equipment) {
    		DB::table('demo_equipment')->insert([
            	'name' => $equipment
        	]);
    	}
    }
}

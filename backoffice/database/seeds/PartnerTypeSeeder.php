<?php

use Illuminate\Database\Seeder;

class PartnerTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $types = array('Distributor', 'DMR', 'National Reseller', 'VAR', 'Rep Firm', 'Other');
        
    	foreach ($types as $type) {
    		DB::table('partner_type')->insert([
            	'name' => $type
        	]);
    	}
    }
}

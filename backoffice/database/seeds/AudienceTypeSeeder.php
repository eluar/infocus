<?php

use Illuminate\Database\Seeder;

class AudienceTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $types = array('End User', 'B2B', 'Reseller', 'Distributor', 'Partner', 'Dealer', 'Customer');
        
    	foreach ($types as $type) {
    		DB::table('audience_type')->insert([
            	'name' => $type
        	]);
    	}
    }
}

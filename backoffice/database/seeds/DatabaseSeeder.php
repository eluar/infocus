<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(AudienceTypeSeeder::class);
        $this->call(DemoEquipmentSeeder::class);
        $this->call(MarketingItemsSeeder::class);
        $this->call(PartnerTypeSeeder::class);
        //$this->call(ProductDisplayedSeeder::class);

        DB::table('users')->insert([
            'name' => 'infocus',
            'email' => 'infocus',
            'password' => bcrypt('portland'),
            'role' => 'manager',
        ]);
    }
}

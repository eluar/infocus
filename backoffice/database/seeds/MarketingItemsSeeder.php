<?php

use Illuminate\Database\Seeder;

class MarketingItemsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $items = array('Table Throw', 'Literature', 'Giveaways', 'Pens', 'Signage',
        				'Raffle give away', 'Supply Kit', 'Literature Rack', 'Other');
        
    	foreach ($items as $item) {
    		DB::table('marketing_items')->insert([
            	'name' => $item
        	]);
    	}
    }
}

<?php

use Illuminate\Database\Seeder;

class ProductDisplayedSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $products = array('JTouch', 'Mondopad', 'Projector', 'DigiEasel', 'CRS 4K', 'Catalyst 4K',
        				'Fusion Catalyst 4500', 'Canvas Touch', 'ConX Exec', 'PixelNet');
        
    	foreach ($products as $product) {
    		DB::table('products_displayed')->insert([
            	'name' => $product
        	]);
    	}
    }
}

<p>
    <b>A new Event Request for InFocus arrived.</b> <br>
    Following is the data regarding that request:
</p>
<table>
	<tr>
		<td>Name:</td>
        <td>{{ $eventModel->your_name }}</td>
	</tr>
    <tr>
		<td>Email:</td>
        <td>{{ $eventModel->email }}</td>
	</tr>
    <tr>
		<td>Parter Name:</td>
        <td>{{ $eventModel->partner_name }}</td>
	</tr>
    <tr>
		<td>Program Name:</td>
        <td>{{ $eventModel->program_name }}</td>
	</tr>
    <tr>
		<td>Cost center:</td>
        <td>{{ $eventModel->cost_center }}</td>
	</tr>
    <tr>
		<td>Event Description:</td>
        <td>{{ $eventModel->description_of_event }}</td>
	</tr>
    <tr>
		<td>Dates:</td>
        <td>From {{ $eventModel->activity_event_date }} To {{ $eventModel->activity_event_date_to }}</td>
	</tr>
    <tr>
		<td>Product displayed:</td>
        <td>{{ $eventModel->product_displayed }}</td>
	</tr>
    {{-- <tr>
		<td>Attending InFocus employees:</td>
        <td>{{ $eventModel->attending_infocus_employees }}</td>
	</tr> --}}
    <tr>
        <td>Company name</td>
        <td>{{ $eventModel->company_name }}</td>
    </tr>
    <tr>
        <td>delivery Address</td>
        <td>{{ $eventModel->street_address }}, {{ $eventModel->city }} {{ $eventModel->province }} {{ $eventModel->state }} {{ $eventModel->zipcode }}, {{$eventModel->country}}</td>
    </tr>
    <tr>
        <td>Contact name</td>
        <td>{{ $eventModel->contact_name }}</td>
    </tr>
    <tr>
        <td>Contact email</td>
        <td>{{ $eventModel->contact_email }}</td>
    </tr>
    <tr>
        <td>Phone number</td>
        <td>{{ $eventModel->phone_number }}</td>
    </tr>
    <tr>
        <td>Date needed</td>
        <td>{{ $eventModel->date_needed }}</td>
    </tr>
    <tr>
        <td>Date can be returned</td>
        <td>{{ $eventModel->date_to_return }}</td>
    </tr>

    <tr>
		<td>Url for event:</td>
        <td>{{ $eventModel->url_for_event }}</td>
	</tr>
    <tr>
		<td>Marketing items:</td>
        <td>{{ $eventModel->marketing_item }}</td>
	</tr>
    <tr>
        <td>Additional items:</td>
        <td>{{ $eventModel->additional_items }}</td>
    </tr>
    <tr>
		<td>Demo equipments:</td>
        <td>{{ $eventModel->demo_equipment }}</td>
	</tr>
    <tr>
        <td>Accessories:</td>
        <td>{{ $eventModel->accessories }}</td>
    </tr>
    {{--<tr>
		<td>Email blast salesforce:</td>
        <td>{{ $eventModel->email_blast_salesforce }}</td>
	</tr>--}}
    <tr>
		<td>Pre-show or Post-show email message:</td>
        <td>{{ $eventModel->event_email_campaign }}</td>
	</tr>
    <tr>
		<td>Additional comments:</td>
        <td>{{ $eventModel->additional_comments }}</td>
	</tr>
</table>

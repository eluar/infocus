<h2>You submited a New Collateral Marketing Request</h2>
<p>Request information is just bellow:</p>
<table border="0">
	<tr bgcolor="#999999">
		<td><b>Item</b></td>
		<td><b>Quantity</b></td>
		<td><b>Notes</b></td>
	</tr>
	<?php $count = 1; ?>
	@foreach($grid as $item)
		<tr bgcolor="{{ ($count % 2) == 0 ? '#cccccc' : '#eeeeee' }}">
			<td>{{ $item['label'] }}</td>
			<td>{{ $item['qty'] }}</td>
			<td>{{ $item['notes'] }}</td>
		</tr>
		<?php $count++; ?>
	@endforeach
	</table>

<table>
	@foreach($flat as $item)
		<tr>
			<td>{{ $item['label'] }} : </td>
			<td>{{ $item['value'] }}</td>
		</tr>
	@endforeach
</table>

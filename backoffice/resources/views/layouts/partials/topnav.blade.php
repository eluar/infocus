<div class="brand clearfix">
    <a href="/" class="logo"><img src="img/logo.jpg" class="img-responsive" alt=""></a>
    <span class="menu-btn"><i class="fa fa-bars"></i></span>
    <ul class="ts-profile-nav">
        {{--not right now--}}
        {{--<li><a href="#">Help</a></li>--}}
        {{--<li><a href="#">Settings</a></li>--}}
        <li class="ts-account">
            <a href="#"><img src="img/ts-avatar.png" class="ts-avatar hidden-side" alt=""> {{ $user->name }} <i class="fa fa-angle-down hidden-side"></i></a>
            <ul>
                {{--not right now--}}
                {{--<li><a href="#">My Account</a></li>--}}
                {{--<li><a href="#">Edit Account</a></li>--}}
                <li><a href="logout">Logout</a></li>
            </ul>
        </li>
    </ul>
</div>

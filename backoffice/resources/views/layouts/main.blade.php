<!doctype html>
<html lang="en" class="no-js">

<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">
	<meta name="theme-color" content="#3e454c">

	<title>@yield("title")</title>

	<!-- Font awesome -->
	<link rel="stylesheet" href="/backoffice/public/css/font-awesome.min.css">
	<!-- Sandstone Bootstrap CSS -->
	<link rel="stylesheet" href="/backoffice/public/css/bootstrap.min.css">
	<!-- Bootstrap Datatables -->
	<link rel="stylesheet" href="/backoffice/public/css/dataTables.bootstrap.min.css">
	<!-- Bootstrap social button library -->
	<link rel="stylesheet" href="/backoffice/public/css/bootstrap-social.css">
	<!-- Bootstrap select -->
	<link rel="stylesheet" href="/backoffice/public/css/bootstrap-select.css">
	<!-- Bootstrap file input -->
	<link rel="stylesheet" href="/backoffice/public/css/fileinput.min.css">
	<!-- Awesome Bootstrap checkbox -->
	<link rel="stylesheet" href="/backoffice/public/css/awesome-bootstrap-checkbox.css">
	<!-- Admin Stye -->
	<link rel="stylesheet" href="/backoffice/public/css/style.css">

	<link rel="stylesheet" href="/backoffice/public/css/jquery-ui.min.css">

	<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Loading Scripts -->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script> 

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.js"></script> 

	<!--<script src="/backoffice/public/js/jquery.min.js"></script>
	<script src="/backoffice/public/js/jquery-ui.min.js"></script>-->
	<script src="/backoffice/public/js/bootstrap-select.min.js"></script>
	<script src="/backoffice/public/js/bootstrap.min.js"></script>
	<script src="/backoffice/public/js/jquery.dataTables.min.js"></script>
	<script src="/backoffice/public/js/dataTables.bootstrap.min.js"></script>
	<script src="/backoffice/public/js/bootstrap-datepicker.min.js"></script>
	<script src="/backoffice/public/js/Chart.min.js"></script>
	<script src="/backoffice/public/js/fileinput.js"></script>
	<script src="/backoffice/public/js/chartData.js"></script>
	<script src="/backoffice/public/js/main.js"></script>

</head>

<body>
	@include("layouts.partials.topnav")

	<div class="ts-main-content">
		@include("layouts.partials.sidebar")
		<div class="content-wrapper">
			<div class="container-fluid">

				@yield('content')

			</div>
		</div>
	</div>

	<script>

	window.onload = function(){
		// Line chart from swirlData for dashReport
		if ($("#dashReport").length > 0) {
			$.get('/backoffice/public/api/get-registrations-chart-data',function(data){
				var ctx = document.getElementById("dashReport").getContext("2d");
				window.myLine = new Chart(ctx).Line(data, {
					responsive: true,
					scaleShowVerticalLines: false,
					scaleBeginAtZero : true,
					multiTooltipTemplate: "<%if (label){%><%=datasetLabel%>: <%}%><%= value %>",
					legend: {display:true}
				});
			});
		}
		// Pie Chart from doughutData
		// var doctx = document.getElementById("chart-area3").getContext("2d");
		// window.myDoughnut = new Chart(doctx).Pie(doughnutData, {responsive : true});

		// Dougnut Chart from doughnutData
		// var doctx = document.getElementById("chart-area4").getContext("2d");
		// window.myDoughnut = new Chart(doctx).Doughnut(doughnutData, {responsive : true});

		$('.datepicker').datepicker({
		    format: 'mm/dd/yyyy'
		});
	}
	</script>

</body>

</html>

<div class="col-md-4">
    <div class="panel panel-default">
        <div class="panel-body bk-primary text-light">
            <div class="stat-panel text-center">
                <div class="stat-panel-number h1 ">{{ $total_events }}</div>
                <div class="stat-panel-title text-uppercase">Total Registered Contacts</div>
            </div>
        </div>
        {{--not right now--}}
        {{--<a href="#" class="block-anchor panel-footer">Full Detail <i class="fa fa-arrow-right"></i></a>
    </div>
</div>
<div class="col-md-4">
    <div class="panel panel-default">
        <div class="panel-body bk-success text-light">
            <div class="stat-panel text-center">
                <div class="stat-panel-number h1 ">8</div>
                <div class="stat-panel-title text-uppercase">Support Tickets</div>
            </div>
        </div>--}}
        {{--not right now--}}
        {{--<a href="#" class="block-anchor panel-footer text-center">See All &nbsp; <i class="fa fa-arrow-right"></i></a>
    </div>
</div>
<div class="col-md-4">
    <div class="panel panel-default">
        <div class="panel-body bk-info text-light">
            <div class="stat-panel text-center">
                <div class="stat-panel-number h1 ">5</div>
                <div class="stat-panel-title text-uppercase">New Something</div>
            </div>
        </div>--}}
        {{--not right now--}}
        {{--<a href="#" class="block-anchor panel-footer text-center">See All &nbsp; <i class="fa fa-arrow-right"></i></a>--}}
    </div>
</div>

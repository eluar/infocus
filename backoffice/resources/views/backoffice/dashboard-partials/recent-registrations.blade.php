<div class="panel panel-default">
    <div class="panel-heading">Recent Registrations</div>
    <div class="panel-body">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Partner Name</th>
                    <th>Event/Program Name</th>
                    <th>Event Dates</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($events as $event)
                    <tr>
                        <th scope="row">{{ $event->id }}</th>
                        <td>{{ $event->your_name }}</td>
                        <td>{{ $event->email }}</td>
                        <td>{{ $event->partner_name }}</td>
                        <td>{{ $event->program_name }}</td>
                        <td>from {{ $event->activity_event_date }} to {{ $event->activity_event_date_to }}</td>
                        <td></td>
                    </tr>
                @endforeach
            </tbody>
            <div class="pagination">{{ $events->links() }}</div>
        </table>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading">Registration Report</div>
    <div class="panel-body">
        <div class="chart">
            <canvas id="dashReport" height="310" width="600"></canvas>
        </div>
        <div id="legendDiv"></div>
    </div>
</div>

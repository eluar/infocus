@extends('layouts.main')

@section('title', 'Dashboard')

@section('content')

<div class="row">
    <div class="col-md-12">

        <h2 class="page-title">Dashboard</h2>

        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    @include("backoffice.dashboard-partials.top-stats")
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                @include("backoffice.dashboard-partials.recent-registrations")
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                @include("backoffice.dashboard-partials.registration-chart")
            </div>
        </div>

        @include("backoffice.dashboard-partials.pie-charts")

    </div>
</div>

@endsection

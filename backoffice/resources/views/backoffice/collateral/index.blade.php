<!-- index.blade.php -->
@extends('layouts.main')

@section('title', 'Collateral List')

@section('content')
  <style type="text/css">
    @media (min-width: 1200px) {
      .container {
          width: 1086px;
      }
    }
  </style>
  <div class="container">
    <div class="col-sm-12 col-lg-8"><a href="/backoffice/public/collateral/new" class="btn btn-info">New Item</a></div>
    <table class="table table-striped form-item-list">
    <thead>
      <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Status</th>
        <th colspan="2">Action</th>
      </tr>
    </thead>
    <tbody>
      @foreach($collaterals as $collateral)
      <tr for="{{ $collateral['id'] }}" id="order_{{ $collateral['id'] }}">
        <td>{{ $collateral['id'] }}</td>
        <td>{{ $collateral['name'] }}</td>
        <td>{{ $collateral['active'] ? 'Active' : 'Inactive' }}</td>
        <td><a href="/backoffice/public/collateral/{{ $collateral['id'] }}" class="btn btn-warning" style="width: 85px">Edit</a></td>
        <td>
          <form action="/backoffice/public/collateral/{{ $collateral['id'] }}" method="post" style="width: 55px;">
            {{csrf_field()}}
            <input name="_method" type="hidden" value="DELETE">
            <button class="btn btn-danger" type="submit">Delete</button>
          </form>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
  </div>

  <script type="text/javascript">
    $( document ).ready(function() {
      var $elToSort = $( ".form-item-list > tbody" );
      $elToSort.sortable();
      var initial = $elToSort.sortable("serialize");
      console.log('initial:', initial);
      $( ".form-item-list > tbody" ).on( "sortstop", function( event, ui ) {
        var current = $elToSort.sortable("serialize");
        if (current == initial) {
          return false;
        }
        $.ajax({
          method: "POST",
          type: "POST",
          url: "/backoffice/public/api/collateral/sort",
          data: current,
          dataType: 'json',
          success: function(dataSuccess,status,xhr) {
            if (dataSuccess.success) {
              console.log('updated to:', current);
              initial = current;
            } else {
              console.error('Error:', dataSuccess);
            }
          },
          error: function( data ) {
            console.error('error:', data);
          }
        });
      });
    });
  </script>

@endsection
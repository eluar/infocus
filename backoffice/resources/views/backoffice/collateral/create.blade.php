<!-- edit.blade.php -->

@extends('layouts.main')
@section('content')
<style type="text/css">
    @media (min-width: 1200px) {
      .container {
          width: 1086px;
      }
    }
</style>
<div class="container">
  <form method="post" action="/backoffice/public/collateral/store" enctype="multipart/form-data">
    <div class="form-group row">
      {{csrf_field()}}
      <input name="_method" type="hidden" value="POST">
      <label for="name" class="col-sm-2 col-lg-8 col-form-label col-form-label-lg">Name</label>
      <div class="col-sm-10">
        <input type="text" class="form-control form-control-lg" id="name" placeholder="Name" name="name" value="">
      </div>
    </div>
    <div class="form-group row">
      <label for="description" class="col-sm-2 col-lg-8 col-form-label col-form-label-sm">Description</label>
      <div class="col-sm-10">
        <textarea name="description" id="description" rows="8" cols="80"></textarea>
      </div>
    </div>
    <div class="form-group row">
      <label for="image" class="col-sm-2 col-lg-8 col-form-label col-form-label-sm">Image</label>
      <div class="col-sm-10">
        <input data-preview="#imagePreview" type="file" id="image" class="form-control form-control-lg" placeholder="Image" name="image">
        <img class="col-sm-6" id="preview"  src="" width="425" ></img>
      </div>
    </div>
    <div class="form-group row">
      <label for="active" class="col-sm-2 col-lg-8 col-form-label col-form-label-sm">Active</label>
      <div class="col-sm-10">
        <input type="checkbox" class="form-control form-control-lg" id="active" name="active">
      </div>
    </div>
    <div class="form-group row">
      <div class="col-md-2"></div>
      <button type="submit" class="btn btn-primary">Create Item</button>
    </div>
  </form>
</div>
@endsection
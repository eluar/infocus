@extends('layouts.main')

@section('title', 'Export CSV File')

@section('content')

<div class="row">
					<div class="col-md-12">
						<h2 class="page-title">Export CSV File</h2>
						<div class="panel panel-default">
							<div class="panel-heading">Select the date range</div>
							<div class="panel-body">
								<form class="form-horizontal" action="./export"  method="post">
									<div class="hr-dashed"></div>
									<div class="form-group">
										{{--<label class="col-sm-2 control-label">Input groups</label>--}}

										<div class="col-sm-10">
											<div class="input-group mb">
												<input type="text" placeholder="Start Date" class="form-control datepicker" name="start_Date">
											</div>
                                            <div class="input-group mb">
												<input type="text" placeholder="Finish Date" class="form-control datepicker" name="end_Date">
											</div>
											<button type="submit" class="btn btn-primary btn-lg">Export CSV File</button>
										</div>
									</div>
									@if($status)
									<div class="alert alert-dismissible alert-{{$status}}">
									    <strong>{{ $message }}</strong>
								    </div>
									@endif
								</form>
							</div>
						</div>

					</div>
				</div>

@endsection

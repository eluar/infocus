<?php
	// Start session.
	session_start();

	if (!$_SESSION['is_member_user']) {
		$_SESSION['refer_to'] = '/';
		header('Location: ./login.php');
	}

	$costCenters = array(
		'Jupiter by InFocus 3400',
	    'InFocus North America 3337',
	    'InFocus EMEA 3533',
	    'Infocus APAC 3349',
	);
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta charset="utf-8" />

		<!-- User defined head content such as meta tags and encoding options -->
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="generator" content="RapidWeaver" />

		<link rel="icon" type="image/vnd.microsoft.icon" href="rw_common/images/favicon.ico">


		<!-- User defined head content -->


		<!-- Meta tags -->
	  	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />

		<title>InFocus Event / Demo Request </title>

		<!-- Load some fonts from Google's Font service -->
		<link href='http://fonts.googleapis.com/css?family=Open Sans:400,300,800|Noto Serif|Arvo' rel='stylesheet' type='text/css'>

	  	<!-- CSS stylesheets reset -->
	  <link rel="stylesheet" type="text/css" media="all" href="rw_common/themes/tesla_pro/consolidated-0.css" />



		<!-- CSS for the Foundation framework's CSS that handles the responsive columnized layout -->


	  <!-- Main Stylesheet -->


	  <!-- Loads Font Awesome v4.0.3 CSS from CDN -->
	  <link href="http://netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">

	  <!--modal pluggin-->
	   <link rel="stylesheet" type="text/css" media="all" href="rw_common/themes/tesla_pro/css/jquery.modal.min.css" />

		<!-- Base RapidWeaver javascript -->

		<!-- jQuery 1.8 is included in the theme internally -->
	  <script src="rw_common/themes/tesla_pro/js/jquery.js"></script>

	  <script type="text/javascript" src="rw_common/themes/tesla_pro/js/jquery.datetimepicker.full.js?<?php echo rand(199,9999);?>"></script>

	  <link rel="stylesheet" type="text/css" href="rw_common/themes/tesla_pro/js/jquery-ui-1.12.1/jquery-ui.min.css" />

	  <link rel="stylesheet" type="text/css" href="rw_common/themes/tesla_pro/css/jquery.datetimepicker.css">

	  <link rel="stylesheet" type="text/css" media="all" href="rw_common/themes/tesla_pro/css/main.css" />

  	  <script type="text/javascript" src="rw_common/themes/tesla_pro/js/jquery-ui-1.12.1/jquery-ui.min.js?<?php echo rand(199,9999); ?>"></script>
	  <!-- MAIN module to save the data in the backend -->
	  <script type="text/javascript" src="rw_common/themes/tesla_pro/js/main.js?<?php echo rand(199,9999); ?>"></script>

	  <!--modal pluggin-->
	  <script type="text/javascript" src="rw_common/themes/tesla_pro/js/jquery.modal.min.js"></script>

	  <!-- Elixir specific javascript, along with jQuery Easing and a few other elements -->
	  <script src="rw_common/themes/tesla_pro/js/elixir.js"></script>

	<!-- Style variations -->
	<script type="text/javascript" src="rw_common/themes/tesla_pro/js/sidebar/sidebar_hidden.js"></script>


	<!-- Plugin injected code -->

	<style media="screen">
		#myNotificationsPopup .message {
			color: #68467D;
		}
	</style>

	</head>

	<body>

		<header role="banner">

			<!-- inFocus Logo -->
			<div id="logo" data-0="opacity: 1;" data-top-bottom="opacity: 0;" data-anchor-target="#logo">
		  	<img src="rw_common/images/infocus_487.png" width="487" height="96" alt="Site logo"/>
			</div>

			<div id="title_wrapper">
					<!-- InFocus Site Title -->
					<h1 id="site_title" data-0="opacity: 1; top:0px;" data-600="opacity: 0; top: 80px;" data-anchor-target="#site_title">

					</h1>

					<!-- InFocus Slogan -->
					<h2 id="site_slogan" data-0="opacity: 1; top:0px;" data-600="opacity: 0; top: 80px;" data-anchor-target="#site_slogan">
						InFocus Event / Demo Request
					</h2>

					<!-- Scroll down button -->
					<div id="scroll_down_button" data-0="opacity: 1; top:0px;" data-400="opacity: 0; top: 100px;" data-anchor-target="#scroll_down_button">
						<i class="fa fa-angle-down"></i>
					</div>
			</div>

			<!-- Top level navigation -->
			<div id="navigation_bar">
				<div class="row site_width">
					<div class="large-12 columns">
						<nav id="top_navigation"><ul><li><a href="./" rel="" id="current">InFocus Event / Demo Request</a></li></ul></nav>
					</div>
				</div>
			</div>

		</header>

		<!-- Sub-navigation -->
		<div id="sub_navigation_bar">
			<div class="row site_width">
				<div class="large-12 columns">
					<nav id="sub_navigation"><ul></ul></nav>
				</div>
			</div>
		</div>


		<!-- Mobile Navigation -->
		<div id="mobile_navigation_toggle">
			<i id="mobile_navigation_toggle_icon" class="fa fa-bars"></i>
		</div>
		<nav id="mobile_navigation">
			<ul><li><a href="./" rel="" id="current">InFocus Event / Demo Request</a></li></ul>
		</nav>


		<!-- Main Content area and sidebar -->
		<div class="row site_width" id="container">

			<!-- ExtraContent Area 1 -->
			<section id="extraContent1">
				<div class="large-12 columns">
					<div id="extraContainer1"></div>
				</div>

				<div class="clearer"></div>
			</section>

			<section id="content"class="large-8 columns">
<h5>Must be submitted two weeks prior to deliverable/event date to guarantee support</h5>
<div class="message-text"></div>
<div class="message-text sub">All Mandatory Fields and Drop Downs must be filled and or selected for the form to be submitted.</div><br />

<form class="rw-contact-form" action="./files/mailer.php" method="post" enctype="multipart/form-data">
	 <div>

	 	<label>Cost Center</label> *<br />
	 	<select class="form-select-field" name="cost_center">
          <option>Please select one option</option>
        	<?php foreach ($costCenters as $costCenter): ?>
          		<option value="<?php echo $costCenter;?>"><?php echo $costCenter;?></option>
        	<?php endforeach; ?>
      	</select>
	 	
	    <span class="error-msg"></span>
		<br />

		<label>Your Name</label> *<br />
		<input class="form-input-field" type="text" value="" name="your_name" size="40"/><br /><span class="error-msg"></span><br />

		<label>Your Email</label> *<br />
		<input class="form-input-field" type="text" value="" name="email" size="40"/><br /><span class="error-msg"></span><br />

		<label>Partner Name</label> *<br />
		<input class="form-input-field" type="text" value="" name="partner_name" size="40"/><br /><span class="error-msg"></span><br />

		<label>Event/Program Name</label> *<br />
		<input class="form-input-field" type="text" value="" name="program_name" size="40"/><br /><span class="error-msg"></span><br />

		<!-- <label>Cost Center</label> *<br />
		<input class="form-input-field" type="text" value="" name="cost_center" size="40"/><br /><span class="error-msg"></span><br /> -->

		<h3>Program Information</h3>

		<label>Description of Event</label> *<br />
		<select class="form-select-field" name="description_of_event">
			<option value="">Select One</option>
			<option value="Trade Show">Trade Show</option>
			<option value="Demo">Demo</option>
			<option value="MDF">MDF</option>
		</select> <br>
		<span class="error-msg"></span><br />
		<!-- <textarea class="form-input-field" name="description_of_event" rows="8" cols="38" placeholder="(Trade Show, Dealer Show, Tabletop, Sales Meeting, Café Day, Breakfast, Lunch, Training, etc…)"></textarea><br /><span class="error-msg"></span><br /> -->

		<label>Activity/Event Date &amp; Time</label> *<br />
		<div class="input-group mb" style="width: 47%; display:inline-block;">
			<input class="form-input-field datepicker" type="text" value="" name="activity_event_date" size="40" placeholder="from" style="width: 80%;"/>
		 	<span class="input-group-addon"><i class="fa fa-table" style="font-size:28px; color:#68467D;"></i></span><br /><span class="error-msg"></span><br />
		 </div>

		<div class="input-group mb" style="width: 47%; display:inline-block;">
 			<input class="form-input-field datepicker" type="text" value="" name="activity_event_date_to" size="40" placeholder="to" style="width: 80%;"/>
 		 	<span class="input-group-addon"><i class="fa fa-table" style="font-size:28px; color:#68467D;"></i></span><br /><span class="error-msg"></span><br />
 		</div>
		<br>

		<label>Web URL for Event (if available)</label> <br />
 		<input class="form-input-field" type="text" value="" name="url_for_event" size="40"/>
		<span class="error-msg"></span> <br /><br />

		<?php /* hidding this on client decision
		<label>Audience Type</label> <br />
		<select class="form-select-field" name="audience_type">
			<option value=""> Select Audience Type</option>
			<option value="End User">End User</option>
			<option value="B2B">B2B</option>
			<option value="Reseller">Reseller</option>
			<option value="Distributor">Distributor</option>
			<option value="Partner">Partner</option>
			<option value="Dealer">Dealer</option>
			<option value="Customer">Customer</option>
		</select><br /><span class="error-msg"></span><br />
		*/?>

		<!-- <input type="checkbox" name="marketing_item" value="Literature" />
			<label>Literature</label> <br /> -->
		<div class="mdf-go-to-collateral" id="collateral-form-wrapper">
			<a href="./rw_common/themes/tesla_pro/partials/collateral-form.php" id="collateral-form" class="mdf-go-to-collateral" style="font-size:13px;">
				For collateral request click here
			</a>
		</div>
		<br />

		<label>Marketing supplies:</label> *<br />

		<input type="checkbox" name="marketing_item" value="No Items Needed" />
			<label>No Items Needed</label><br />
		<input type="checkbox" name="marketing_item" value="All" />
			<label>All</label><br />
		<input type="checkbox" name="marketing_item" value="Table Throw" />
			<label>Table Throw</label><br />

		<input type="checkbox" name="marketing_item" value="Marketing supply case: 7’x3’ retractable product signs, Table throws, Tool kit, Supply box, Literature stand, Raffle box, Mints in a jar" />
		<label>Marketing supply case - Contains:</label><br />

		<ul>
			<li>7’x3’ retractable product signs</li>
			<li>Table throws</li>
			<li>Tool kit</li>
			<li>Supply box</li>
			<li>Literature stand</li>
			<li>Raffle box</li>
			<li>Mints in a jar</li>
		</ul>
		<br />
		<span class="error-msg" id="marketing_item-error"></span> <br>

		<label>Additional Items Available:</label> <br />

		<input type="checkbox" name="additional_items" value="First aid kit" />
		<label>First aid kit</label><br />

		<input type="checkbox" name="additional_items" value="Giveaways" />
		<label>Giveaways (100 QTY)</label><br />

		<input type="checkbox" name="additional_items" value="Pens" />
		<label>Pens (100 QTY)</label><br />

		<input type="checkbox" name="additional_items" value="Raffle give away" />
		<label>Raffle give away</label><br />

		<input type="checkbox" name="additional_items" value="Signage" id="signage-items" />
		<label>Signage</label><br />

		<ul class="additional-items-signage">
			<li>
				<input type="checkbox" name="additional_items_signage" value="Projectors" disabled="disabled" />
				<label>Projectors</label>
			</li>
			<li>
				<input type="checkbox" name="additional_items_signage" value="Interactive displays" disabled="disabled" />
				<label>Interactive displays</label>
			</li>
			<li>
				<input type="checkbox" name="additional_items_signage" value="Collaborations Visualization products" disabled="disabled" />
				<label>Collaborations Visualization products</label>
			</li>
		</ul>
		<br />

		<!-- <input <?php ?> type="checkbox" name="marketing_item" value="Supply Kit" />
			<label>Supply Kit</label><br /> -->

		<!-- <input <?php ?> type="checkbox" name="marketing_item" value="Literature Rack" />
			<label>Literature Rack</label><br /> -->

		<!-- <input type="checkbox" name="marketing_item" value="Other" />
			<label>Other</label>
			<input type="text" class="form-input-field other" style="width:10em;" value="" id="marketing_item_other" width="15"><br /> -->

		<span class="error-msg" id="additional_items-error"></span>
<br />


		<label>Demo Equipment Needed:</label> *<br />
		<!-- <input type="checkbox" name="demo_equipment" value="All" />
			<label>All</label><br /> -->
		<input type="checkbox" name="demo_equipment" value="JTouch" />
			<label>JTouch</label><br />

		<input type="checkbox" name="demo_equipment" value="Monopad" />
			<label>Monopad</label><br />

		<input type="checkbox" name="demo_equipment" value="Projector" />
			<label>Projector</label><br />

		<input type="checkbox" name="demo_equipment" value="DigiEasel" />
			<label>DigiEasel</label><br />

		<input type="checkbox" name="demo_equipment" value="CRS 4K" />
			<label>CRS 4K</label><br />

		<input type="checkbox" name="demo_equipment" value="Catalyst 4K" />
			<label>Catalyst 4K</label><br />

		<input type="checkbox" name="demo_equipment" value="Fusion Catalyst 4500" />
			<label>Fusion Catalyst 4500</label><br />

		<input type="checkbox" name="demo_equipment" value="Canvas Touch" />
			<label>Canvas Touch</label><br />

		<input type="checkbox" name="demo_equipment" value="ConX Exec" />
			<label>ConX Exec</label><br />

		<input type="checkbox" name="demo_equipment" value="PixelNet" />
			<label>PixelNet</label><br />

		<input type="checkbox" name="demo_equipment" value="PixelNet" />
			<label>Control 1000</label><br />

		<!-- <input 'type="checkbox" name="demo_equipment" value="Model No." /> -->
		<!-- <label>Model No.</label>
		<input type="text" class="form-input-field model-no" style="width:6em;" value="" id="demo_equipment_model" width="15"><br /> -->


		<span class="error-msg" id="demo_equipment-error"></span> <br>


		<label>Accessories:</label> *<br />
		
		<input type="checkbox" name="accessories" value="None Needed" />
			<label>None Needed</label><br />

		<input type="checkbox" name="accessories" value="LiteShow 4" />
			<label>LiteShow 4</label><br />

		<input type="checkbox" name="accessories" value="LiteCast Key" />
			<label>LiteCast Key</label><br />

		<input type="checkbox" name="accessories" value="PTZ4 Camera" />
			<label>PTZ4 Camera</label><br />

		<input type="checkbox" name="accessories" value="Camera and Mic Array" />
			<label>Camera and Mic Array</label><br />

		<input type="checkbox" name="accessories" value="Android Whiteboard Module" />
			<label>Android Whiteboard Module</label><br />

		<input type="checkbox" name="accessories" value="Soundbar" />
			<label>Soundbar</label><br />

		<input type="checkbox" name="accessories" value="Thunder Speaker Phone" />
			<label>Thunder Speaker Phone</label><br />

		<input type="checkbox" name="accessories" value="Thunder Mic Pods" />
			<label>Thunder Mic Pods</label><br />


		<span class="error-msg" id="accessories-error"></span> <br>


		<h3>Shipping Information</h3>

		<label>Event/Company Name</label> *<br />
		<input class="form-input-field" type="text" value="" name="company_name" size="40"/><br /><span class="error-msg"></span><br />

		<!-- <label>Delivery address</label> *<br /> -->
		<input class="form-input-field" type="hidden" value=", " name="delivery_address" size="40"/><span class="error-msg"></span>

		<label>Delivery address</label> *<br />
		<input class="form-input-field" type="text" value="" name="street_address" size="40"/><br /><span class="error-msg"></span><br />

		<label>City</label> *<br />
		<input class="form-input-field" type="text" value="" name="city" size="40"/><br /><span class="error-msg"></span><br />

		<label style="display:none;" id="province_label">Province</label> <br />
		<input class="form-input-field" type="text" value="" id="province" name="province" size="40" style="display:none;"/><br /><span class="error-msg"></span><br />

		<label class="state_label">State *</label> <br />
		<select class="form-select-field" name="state" id="state">
			<option value="Alabama">Alabama</option>
			<option value="Alaska">Alaska</option>
			<option value="Arizona">Arizona</option>
			<option value="Arkansas">Arkansas</option>
			<option value="California">California</option>
			<option value="Colorado">Colorado</option>
			<option value="Connecticut">Connecticut</option>
			<option value="Delaware">Delaware</option>
			<option value="District of Columbia">District of Columbia</option>
			<option value="Florida">Florida</option>
			<option value="Georgia">Georgia</option>
			<option value="Hawaii">Hawaii</option>
			<option value="Idaho">Idaho</option>
			<option value="Illinois">Illinois</option>
			<option value="Indiana">Indiana</option>
			<option value="Iowa">Iowa</option>
			<option value="Kansas">Kansas</option>
			<option value="Kentucky">Kentucky</option>
			<option value="Louisiana">Louisiana</option>
			<option value="Maine">Maine</option>
			<option value="Maryland">Maryland</option>
			<option value="Massachsetts">Massachsetts</option>
			<option value="Michigan">Michigan</option>
			<option value="Minnesota">Minnesota</option>
			<option value="Mississippi">Mississippi</option>
			<option value="Missouri">Missouri</option>
			<option value="Montana">Montana</option>
			<option value="Nebraska">Nebraska</option>
			<option value="Nevada">Nevada</option>
			<option value="New Hampshire">New Hampshire</option>
			<option value="New Jersey">New Jersey</option>
			<option value="New Mexico">New Mexico</option>
			<option value="New York">New York</option>
			<option value="North Carolina">North Carolina</option>
			<option value="North Dakota">North Dakota</option>
			<option value="Ohio">Ohio</option>
			<option value="Oklahoma">Oklahoma</option>
			<option value="Oregon">Oregon</option>
			<option value="Pennsylvania">Pennsylvania</option>
			<option value="Rhode Island">Rhode Island</option>
			<option value="South Carolina">South Carolina</option>
			<option value="South Dakota">South Dakota</option>
			<option value="Tennessee">Tennessee</option>
			<option value="Texas">Texas</option>
			<option value="Utah">Utah</option>
			<option value="Vermont">Vermont</option>
			<option value="Virginia">Virginia</option>
			<option value="Washington">Washington</option>
			<option value="West Virginia">West Virginia</option>
			<option value="Wisconsin">Wisconsin</option>
			<option value="Wyoming">Wyoming</option>
		</select><br />
		<span class="state">Required if United States of America is selected</span>
		<span class="error-msg"></span><br />


<?php	$countries = array('Afganistan','Albania','Algeria','American Samoa','Andorra','Angola','Anguilla',
		'Antigua &amp; Barbuda','Argentina','Armenia','Aruba','Australia','Austria','Azerbaijan','Bahamas',
		'Bahrain','Bangladesh','Barbados','Belarus','Belgium','Belize','Benin','Bermuda','Bhutan','Bolivia',
		'Bonaire','Bosnia &amp; Herzegovina','Botswana','Brazil','British Indian Ocean Ter','Brunei','Bulgaria',
		'Burkina Faso','Burundi','Cambodia','Cameroon','Canada','Canary Islands','Cape Verde','Cayman Islands',
		'Central African Republic','Chad','Channel Islands','Chile','China','Christmas Island','Cocos Island',
		'Colombia','Comoros','Congo','Cook Islands','Costa Rica','Cote DIvoire','Croatia','Cuba','Curaco','Cyprus',
		'Czech Republic','Denmark','Djibouti','Dominica','Dominican Republic','East Timor','Ecuador','Egypt',
		'El Salvador','Equatorial Guinea','Eritrea','Estonia','Ethiopia','Falkland Islands','Faroe Islands','Fiji',
		'Finland','France','French Guiana','French Polynesia','French Southern Ter','Gabon','Gambia','Georgia',
		'Germany','Ghana','Gibraltar','Great Britain','Greece','Greenland','Grenada','Guadeloupe','Guam','Guatemala',
		'Guinea','Guyana','Haiti','Hawaii','Honduras','Hong Kong','Hungary','Iceland','India','Indonesia','Iran','Iraq',
		'Ireland','Isle of Man','Israel','Italy','Jamaica','Japan','Jordan','Kazakhstan','Kenya','Kiribati','Korea North',
		'Korea Sout','Kuwait','Kyrgyzstan','Laos','Latvia','Lebanon','Lesotho','Liberia','Libya','Liechtenstein',
		'Lithuania','Luxembourg','Macau','Macedonia','Madagascar','Malaysia','Malawi','Maldives','Mali','Malta',
		'Marshall Islands','Martinique','Mauritania','Mauritius','Mayotte','Mexico','Midway Islands','Moldova','Monaco',
		'Mongolia','Montserrat','Morocco','Mozambique','Myanmar','Nambia','Nauru','Nepal','Netherland Antilles',
		'Netherlands','Nevis','New Caledonia','New Zealand','Nicaragua','Niger','Nigeria','Niue','Norfolk Island',
		'Norway','Oman','Pakistan','Palau Island','Palestine','Panama','Papua New Guinea','Paraguay','Peru',
		'Phillipines','Pitcairn Island','Poland','Portugal','Puerto Rico','Qatar','Republic of Montenegro',
		'Republic of Serbia','Reunion','Romania','Russia','Rwanda','St Barthelemy','St Eustatius','St Helena',
		'St Kitts-Nevis','St Lucia','St Maarten','St Pierre &amp; Miquelon','St Vincent &amp; Grenadines','Saipan',
		'Samoa','Samoa American','San Marino','Sao Tome &amp; Principe','Saudi Arabia','Senegal','Serbia','Seychelles',
		'Sierra Leone','Singapore','Slovakia','Slovenia','Solomon Islands','Somalia','South Africa','Spain','Sri Lanka',
		'Sudan','Suriname','Swaziland','Sweden','Switzerland','Syria','Tahiti','Taiwan','Tajikistan','Tanzania','Thailand',
		'Togo','Tokelau','Tonga','Trinidad &amp; Tobago','Tunisia','Turkey','Turkmenistan','Turks &amp; Caicos Is','Tuvalu',
		'Uganda','Ukraine','United Arab Erimates','United Kingdom','United States of America','Uraguay','Uzbekistan','Vanuatu',
		'Vatican City State','Venezuela','Vietnam','Virgin Islands (Brit)','Virgin Islands (USA)','Wake Island',
		'Wallis &amp; Futana Is','Yemen','Zaire','Zambia','Zimbabwe');
?>
		<label>Country</label> *<br />
		<select class="form-select-field" name="country" id="country">
			<option value="">Select one</option>
			<?php foreach ($countries as $country) {
				$selected = ($country == 'United States of America') ? 'selected' : '';
				echo "<option value=\"{$country}\" {$selected} > {$country} </option>";
				// echo "<option value='" . $country . "'>" . $country . "</option>";
			} ?>
		</select><br /><span class="error-msg"></span><br />


		<label>Zipcode</label> *<br />
		<input class="form-input-field" type="text" value="" name="zipcode" size="40"/><br /><span class="error-msg"></span><br />

		<label>Point of Contact name</label> *<br />
		<input class="form-input-field" type="text" value="" name="contact_name" size="40"/><br /><span class="error-msg"></span><br />

		<!-- <label>Contact email</label> <br />
		<input class="form-input-field" type="text" name="contact_email" size="40"/><br /><span class="error-msg"></span><br /> -->

		<label>Point of Contact Phone Number</label> <br />
		<input class="form-input-field" type="text" value="" name="phone_number" size="40"/><br /><span class="error-msg"></span><br />

		<label>Delivery Date &amp; Time</label> *<br />
		<div class="input-group mb" style="width: 47%; display:inline-block;">
			<input class="form-input-field datepicker" type="text" value="" name="date_needed" size="40" placeholder="from" style="width: 80%;"/>
		 	<span class="input-group-addon"><i class="fa fa-table" style="font-size:28px; color:#68467D;"></i></span><br /><span class="error-msg"></span><br />
		</div> <br>

		 <label>Return Date &amp; Time</label> *<br />
		 <div class="input-group mb" style="width: 47%; display:inline-block;">
 			<input class="form-input-field datepicker" type="text" value="" name="date_to_return" size="40" placeholder="to" style="width: 80%;"/>
 		 	<span class="input-group-addon"><i class="fa fa-table" style="font-size:28px; color:#68467D;"></i></span><br /><span class="error-msg"></span><br />
		</div> <br>
<br />

		<!-- <label>Will you require a pre-show or post-show email message?</label> *<br />
		<input type="radio" name="event_email_campaign" value="Yes" />
			<label>Yes</label><br />
		<input type="radio" name="event_email_campaign" value="No" />
			<label>No</label><br />
		* If you selected yes, I will call you to follow up on details of your requirement.
		<span class="error-msg"></span><br /> -->

		<label>Additional Comments</label> <br />
		<textarea class="form-input-field" name="additional_comments" rows="8" cols="38"></textarea><br /><span class="error-msg"></span><br />

		<div style="display: none;">
			<label>Spam Protection: Please don't fill this in:</label>
			<textarea name="comment" rows="1" cols="1"></textarea>
		</div>
		<input type="hidden" name="form_token" value="<?php echo $security_token; ?>" />
		<input class="form-input-button" type="reset" name="resetButton" value="Reset" />
		<input class="form-input-button" type="submit" name="submitButton" value="Submit" />
		<a href="#" id="logout"> Logout </a>
	</div>
</form>

<br />
<div class="form-footer"></div><br />


			</section>
			<aside id="sidebar" class="large-4 columns">
				<h4 id="sidebar_title"></h4>
				<div id="sidebar_content"></div>
				<div id="archives">

				</div>
			</aside>
		</div>

		<!-- ExtraContent Area 2 -->
		<section id="extraContent2" class="row site_width">
			<div class="large-12 columns">
				<div id="extraContainer2"></div>
			</div>

			<div class="clearer"></div>
		</section>

		<!-- Footer -->
		<footer class="row site_width">
			<div id="footer_content" class="large-12 columns">
				<!-- Social Badges -->
				<div id="social_badges"></div>
				<!-- Breadcrumb Trail -->
				<div id="breadcrumb_container">
					<i class="fa fa-folder-open-o"></i> <span id="breadcrumb"></span>
				</div>
				&copy; <?php echo date('Y'); ?> InFocus
			</div>
		</footer>

		<div id="scroll_up_button"><i class="fa fa-angle-up"></i></div>

		<!-- Modal -->
		<div id="myNotificationsPopup" class="reveal-modal medium" style="display: none">
			<h4 class="message">message.</h4>
		</div>

		<!-- Handles loading Skrollr, which helps in animating portions of the header area. -->
		<!-- We check to see if the user is on an mobile device or not, and only serve up -->
		<!-- the animations on non-mobile devices. -->
		<script>
		 	$elixir(window).load(function() {
			  if(!(/Android|iPhone|iPad|iPod|BlackBerry|Windows Phone/i).test(navigator.userAgent || navigator.vendor || window.opera)){
			      skrollr.init({
			          forceHeight: false
			      });
			  }
			});

			$elixir(document).ready(function(){
				$elixir(".fa-table").on("click", function() {
					// $elixir(this).parent().parent().find(".datepicker").datepicker("show");
					$elixir(this).parent().parent().find(".datepicker").datetimepicker("show");
				});
			});

		</script>

	</body>

</html>
